FROM node:alpine as dependencies
WORKDIR /web
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

FROM node:alpine as builder
WORKDIR /web
COPY . .
COPY --from=dependencies /web/node_modules ./node_modules
RUN yarn build

FROM node:alpine as runner
WORKDIR /web
COPY --from=builder /web/next.config.js ./
COPY --from=builder /web/public ./public
COPY --from=builder /web/.next ./.next
COPY --from=builder /web/node_modules ./node_modules
COPY --from=builder /web/package.json ./package.json

EXPOSE 3000
CMD ["yarn", "start"]