export const phoneRegExp =
	/^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
export const cpfCnpjRegExp =
	/(^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$)/;
export const cpfRegExp = /^\d{3}\.\d{3}\.\d{3}\-\d{2}$/;
export const cnpjRegExp = /^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/;

import axios, { AxiosResponse } from 'axios';
import moment, { Moment } from 'moment';
import { EnumLocale } from './enum';

export class Data {
	private _moment: Moment;
	public constructor(private locale = EnumLocale.ptBr) {
		moment.locale(locale);
		this._moment = moment();
	}
	now(format?: string) {
		if (format) return this._moment.format(format);
		else return this._moment.format();
	}
	getDefault(date: string) {
		return moment(date).format();
	}
	getAge(date: string) {
		var dob = new Date(date);
		var diff_ms = Date.now() - dob.getTime();
		var age_dt = new Date(diff_ms);
		return Math.abs(age_dt.getUTCFullYear() - 1970);
	}
}

export const obterEstados = async () =>
	await axios
		.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
		.then((response) => response.data);

export type IEndereco = {
	cep?: string;
	logradouro?: string;
	complemento?: string;
	bairro?: string;
	localidade?: string;
	uf?: string;
	numero?: string;
};
export const obterEndereco = async ({ cep }: IEndereco) => {
	if (cep && cep.length === 8) {
		return await axios
			.get(`https://viacep.com.br/ws/${cep}/json/unicode/`)
			.then(({ data }: AxiosResponse<any>) => data as IEndereco)
			.catch((err) => console.error(err.message));
	}
};
