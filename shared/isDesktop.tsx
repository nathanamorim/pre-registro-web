import { useMediaQuery } from '@material-ui/core';
export const isDesktop = () => useMediaQuery('(min-width:600px)');
