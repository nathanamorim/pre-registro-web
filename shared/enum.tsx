export enum EnumSexo {
	Masculino = 'M',
	Feminino = 'F',
}

export enum EnumLocale {
	ptBr = 'pt-br',
}

export enum EnumNaturalidadeNascimento {
	Nenhum,
	LocalNascimento,
	EnderecoMae,
}

export enum EnumTipoLocalNascimento {
	Nenhum,
	Hospital,
	EstabelecimentoSaude,
	Domicilio,
	ViaPublica,
	Outro,
}
