import { createContext, useContext } from 'react';

export type GlobalContextType = {
	activedStep: number;
	setActivedStep: (step: number) => void;
};

export const GlobalContext = createContext<GlobalContextType>({
	activedStep: 0,
	setActivedStep: () => {},
});

export const useGlobalContext = () => useContext(GlobalContext);
