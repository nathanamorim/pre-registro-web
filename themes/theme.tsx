import { createMuiTheme } from '@material-ui/core/styles';

declare module '@material-ui/core/styles/createMuiTheme' {
	interface ThemeOptions {
		themeName?: string;
	}
}

const palette = {
	primary: {
		main: '#7dcd52',
		light: '#caecb7',
		dark: '#579834',
	},
	secondary: {
		main: '#FDDC5C',
		light: '#FDDC5C',
	},
	text: {
		primary: '#212529',
		secondary: '#6c757d',
	},
	default: '#212529',
	white: '#FFFFFF',
	grey: {
		50: '#f8f9fa',
	},
};

const themeName = 'default';

export default createMuiTheme({ palette, themeName });
