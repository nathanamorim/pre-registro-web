import React from 'react';
import Link from 'next/link';
import ExternalLayout from '../layouts/ExternalLayout';
import { Button, Container, Grid } from '@material-ui/core';

const Home = () => {
	const [casamento, setCasamento] = React.useState<boolean>(true);
	return (
		<Container>
			<Grid
				spacing={4}
				container
				style={{ marginTop: '2rem' }}
				alignItems="center"
				alignContent="center"
			>
				<Grid item xs={12} md={3}>
					<Link href="/nascimento">
						<Button color="primary" variant="contained" fullWidth>
							{' '}
							Pré registro de Nascimento
						</Button>
					</Link>
				</Grid>
				<Grid item xs={12} md={3}>
					<Link href="/casamento">
						<Button color="primary" variant="contained" fullWidth>
							{' '}
							Pré registro de Casamento
						</Button>
					</Link>
				</Grid>
				<Grid item xs={12} md={3}>
					<Link href="/obito">
						<Button color="primary" variant="contained" fullWidth>
							{' '}
							Pré registro de Óbito
						</Button>
					</Link>
				</Grid>
			</Grid>
		</Container>
	);
};

Home.layout = ExternalLayout;
export default Home;
