import { Container, Grid, TextField } from '@material-ui/core';
import React from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { IFormFieldsWithSetValue } from '../interfaces/IForm';
import ExternalLayout from '../layouts/ExternalLayout';
import { IEndereco, obterEndereco } from '../shared/utils';

const EnderecoPage = () => {
	const { handleSubmit, control, watch, reset, setFocus, setValue, formState } =
		useForm<any>({
			mode: 'onSubmit',
			defaultValues: {},
			// resolver: yupResolver(CasamentoValidation),
		});
	const { errors } = formState;
	const onSubmit: SubmitHandler<any> = async (data) => console.log(data);
	return (
		<Endereco
			control={control}
			setValue={setValue}
			watch={watch}
			errors={errors}
			node={'node'}
		/>
	);
};

const Endereco = ({
	control,
	setValue,
	watch,
	errors,
	node,
}: IFormFieldsWithSetValue) => {
	const watchCep = watch(`${node}.endereco.cep`);
	React.useEffect(() => carregarEndereco(watchCep), [watchCep]);

	const carregarEndereco = (cep: string): void => {
		obterEndereco({ cep: cep }).then((data: IEndereco | void) => {
			setValue(`${node}.endereco.logradouro`, data?.logradouro);
			setValue(`${node}.endereco.bairro`, data?.bairro);
			setValue(`${node}.endereco.localidade`, data?.localidade);
			setValue(`${node}.endereco.uf`, data?.uf);
		});
	};
	return (
		<Container>
			<Grid container spacing={3}>
				<Grid item xs={12} md={3}>
					<Controller
						name={`${node}.endereco.cep`}
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors?.[node]?.endereco?.cep}
								helperText={errors?.[node]?.endereco?.cep?.message}
								label="CEP"
								id="endereco-cep"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={7}>
					<Controller
						name={`${node}.endereco.logradouro`}
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors?.[node]?.endereco?.logradouro}
								helperText={errors?.[node]?.endereco?.logradouro?.message}
								label="Logradouro"
								id="endereco-logradouro"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={2}>
					<Controller
						name={`${node}.endereco.numero`}
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors?.[node]?.endereco?.numero}
								helperText={errors?.[node]?.endereco?.numero?.message}
								label="Número"
								id="endereco-numero"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name={`${node}.endereco.complemento`}
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors?.[node]?.endereco?.complemento}
								helperText={errors?.[node]?.endereco?.complemento?.message}
								label="Complemento"
								id="endereco-complemento"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={2}>
					<Controller
						name={`${node}.endereco.uf`}
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors?.[node]?.endereco?.uf}
								helperText={errors?.[node]?.endereco?.uf?.message}
								label="Estado"
								id="endereco-estado"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>

				<Grid item xs={12} md={3}>
					<Controller
						name={`${node}.endereco.localidade`}
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors?.[node]?.endereco?.localidade}
								helperText={errors?.[node]?.endereco?.localidade?.message}
								label="Cidade"
								id="endereco-cidade"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={3}>
					<Controller
						name={`${node}.endereco.bairro`}
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors?.[node]?.endereco?.bairro}
								helperText={errors?.[node]?.endereco?.bairro?.message}
								label="Bairro"
								id="endereco-bairro"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
			</Grid>
		</Container>
	);
};
EnderecoPage.layout = ExternalLayout;
export default EnderecoPage;
