// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';
type Data = {
	name: string;
};

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse<Data>
) {
	if (req.method === 'POST')
		await axios
			.post('http://localhost:5001/api/nascimentos', req.body)
			.then((response) => console.log(response))
			.catch((err) => console.log(err));
	res.status(200).json({ name: 'John Doe' });
}
