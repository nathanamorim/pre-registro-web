import React from 'react';
import { useRouter } from 'next/router';
import { Button, Container, Fab, Grid } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import ExternalLayout from '../../layouts/ExternalLayout';
import RequerenteForm from '../../components/NascimentoForm/RequerenteForm';
import RegistradoForm from '../../components/NascimentoForm/RegistradoForm';
import FiliacaoForm from '../../components/FiliacaoForm';
import AvosForm from '../../components/NascimentoForm/AvosForm';
import ResultForm from '../../components/ResultForm';
import { useForm, SubmitHandler } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { IFormStepItem } from '../../interfaces/IFormStepItem';
import { isDesktop } from '../../shared/isDesktop';
import Steppers from '../../components/Steppers';
import { ptShort } from 'yup-locale-pt';
import { Data } from '../../shared/utils';
import { INascimento } from '../../interfaces/Nascimento';
import { EnumLocale, EnumSexo } from '../../shared/enum';
import { Guid } from 'guid-typescript';

const stepsNascimento: IFormStepItem[] = [
	{ label: 'Registrado', route: 'registrado' },
	{ label: 'Filiação', route: 'filiacao' },
	{ label: 'Requerente', route: 'requerente' },
	{ label: 'Finalizado', route: 'finalizado' },
];

const WizzardPage = () => {
	const [activeStep, setActiveStep] = React.useState(0);
	const [chave, setChave] = React.useState('');
	const router = useRouter();
	const [completed, setCompleted] = React.useState<{ [k: number]: boolean }>(
		{}
	);

	yup.setLocale(ptShort);
	const formSchema = yup.object().shape({
		registrado: yup.object().shape({
			dnv: yup.string().required().min(3),
			nome: yup.string().required().min(3),
			sexo: yup.string().required(),
			tipoLocal: yup.string().required(),
			naturalidade: yup.string().required(),
		}),
		filiacao: yup.object().shape({
			mae: yup.object().shape({
				nome: yup.string().required().min(3),
				cpf: yup.string().required().min(11),
			}),
		}),
	});
	const data = new Data(EnumLocale.ptBr);
	const [forms, setForms] = React.useState<IFormStepItem[]>(stepsNascimento);
	const { handleSubmit, control, watch, reset, setFocus, formState, setValue } =
		useForm<INascimento>({
			mode: 'onBlur',
			defaultValues: {
				registrado: {
					dnv: '',
					nome: '',
					sexo: EnumSexo.Masculino,
					dataNascimento: data.now('yyyy-MM-DDTHH:mm'),
					gemeos: false,
					endereco: '',
				},
				requerente: {
					cpf: '',
					email: '',
					parentesco: '',
					telefone: '',
				},
				filiacao: {},
			},
			// resolver: yupResolver(formSchema),
		});

	const { errors } = formState;
	const onSubmit: SubmitHandler<INascimento> = async (data) => {
		console.log(data);

		// setActiveStep(stepsNascimento.length - 1);
		// setChave(Guid.create().toString());
	};

	const totalSteps = () => {
		return stepsNascimento.length;
	};

	const completedSteps = () => {
		return Object.keys(completed).length;
	};

	const isLastStep = () => {
		return activeStep === totalSteps() - 1;
	};

	const allStepsCompleted = () => {
		return completedSteps() === totalSteps();
	};

	const handleNext = () => {
		const newActiveStep =
			isLastStep() && !allStepsCompleted()
				? // It's the last step, but not all steps have been completed,
				  // find the first step that has been completed
				  stepsNascimento.findIndex((step, i) => !(i in completed))
				: activeStep + 1;
		setActiveStep(newActiveStep);
		if (newActiveStep === stepsNascimento.length - 1) alert('Enviado');
	};

	const handleBack = () => {
		if (activeStep === stepsNascimento.length - 1) router.push('/');
		else setActiveStep((prevActiveStep) => prevActiveStep - 1);
	};

	const isActive = (route: string) => {
		return !(stepsNascimento.find((s, i) => i === activeStep)?.route === route);
	};
	React.useEffect(() => {
		let valid = !formState.isValid;
		let active = activeStep;
		let size = stepsNascimento.length - 2;
	}, [activeStep]);

	const disabledSubmit =
		!formState.isValid && !(activeStep === stepsNascimento.length - 1);

	const buildButtons = () => {
		return (
			isDesktop() && (
				<Grid
					container
					direction={'row'}
					spacing={2}
					style={{ padding: '2rem 2rem' }}
				>
					<Grid item>
						<Button
							disabled={
								activeStep === 0 || activeStep === stepsNascimento.length - 1
							}
							color="primary"
							variant="contained"
							onClick={handleBack}
						>
							<NavigateBeforeIcon />
						</Button>
					</Grid>
					<Grid item>
						<Button
							variant="contained"
							color="primary"
							onClick={handleNext}
							disabled={disabledNext()}
						>
							<NavigateNextIcon />
						</Button>
					</Grid>
				</Grid>
			)
		);
	};
	const disabledNext = () =>
		activeStep === totalSteps() - 1 || activeStep === totalSteps() - 2;

	return (
		<Container>
			<Steppers
				formSteps={stepsNascimento}
				step={activeStep}
				nextDisabled={disabledNext()}
				next={handleNext}
				prev={handleBack}
			/>
			<div style={{ marginTop: 10 }}>
				<form>
					<Grid
						spacing={2}
						container
						direction="column"
						alignItems="stretch"
						style={{ marginBottom: '1vh' }}
					>
						{buildButtons()}
						<Grid item>
							<RequerenteForm
								errors={errors}
								control={control}
								active={isActive}
								current={activeStep}
								watch={watch}
							/>
							<RegistradoForm
								errors={errors}
								control={control}
								active={isActive}
								current={activeStep}
								watch={watch}
							/>
							<FiliacaoForm
								errors={errors}
								control={control}
								active={isActive}
								current={activeStep}
								watch={watch}
								setValue={setValue}
							/>
							<AvosForm
								errors={errors}
								control={control}
								active={isActive}
								current={activeStep}
								watch={watch}
							/>
							<ResultForm
								chave={chave}
								active={isActive}
								current={activeStep}
							/>
						</Grid>
						{activeStep === stepsNascimento.length - 2 && (
							<Grid item style={{ height: '10vh', position: 'relative' }}>
								<Fab
									color="primary"
									aria-label="salvar"
									style={{ position: 'absolute', right: 30 }}
									onClick={handleSubmit(onSubmit)}
									disabled={disabledSubmit}
								>
									<SaveIcon color={'action'} />
								</Fab>
							</Grid>
						)}
					</Grid>
				</form>
			</div>
		</Container>
	);
};

WizzardPage.layout = ExternalLayout;
export default WizzardPage;
