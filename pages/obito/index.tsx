import React from 'react';
import { useRouter } from 'next/router';
import { Button, Container, Fab, Grid } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import ExternalLayout from '../../layouts/ExternalLayout';
import { useForm, SubmitHandler, SubmitErrorHandler } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { IFormStepItem } from '../../interfaces/IFormStepItem';
import { isDesktop } from '../../shared/isDesktop';
import Steppers from '../../components/Steppers';
import { Data } from '../../shared/utils';
import { EnumLocale } from '../../shared/enum';
import { Guid } from 'guid-typescript';
import ResultForm from '../../components/ResultForm';
import AvosForm from '../../components/AvosForm';
import FiliacaoForm from '../../components/FiliacaoForm';
import { ICasamento } from '../../interfaces/Casamento';
import ToastComponent from '../../components/ToastComponent';
import { FalecimentoForm, RegistradoForm } from '../../components/ObitoForm';
import RequerenteForm from '../../components/ObitoForm/RequerenteForm';

const stepsObito: IFormStepItem[] = [
	{ label: 'Dados do Requerente', route: 'requerente' },
	{ label: 'Dados do Registrado', route: 'registrado' },
	{ label: 'Dados do Falecimento', route: 'falecimento' },
	{ label: 'Filiação', route: 'filiacao' },
	{ label: 'Avós', route: 'avos' },
	{ label: 'Finalizado', route: 'finalizado' },
];

const WizzardPageObito = () => {
	const [activeStep, setActiveStep] = React.useState(0);
	const [chave, setChave] = React.useState('');
	const router = useRouter();
	const [completed, setCompleted] = React.useState<{ [k: number]: boolean }>(
		{}
	);

	const data = new Data(EnumLocale.ptBr);
	const [forms, setForms] = React.useState<IFormStepItem[]>(stepsObito);
	const [toast, setToast] = React.useState(false);
	const { handleSubmit, control, watch, reset, setFocus, setValue, formState } =
		useForm<any>({
			mode: 'onBlur',
			defaultValues: {},
			// resolver: yupResolver(CasamentoValidation),
		});

	const { errors } = formState;

	const onSubmit: SubmitHandler<any> = async (data) => {
		// console.log(data);
		// let response = await axios
		// 	.post(
		// 		'http://localhost:5080/api/nascimentos',
		// 		{ dados: data },
		// 		{ headers: { 'Access-Control-Allow-Origin': '*' } }
		// 	)
		// 	.then((response) => {
		// 		if (response.data.id) {
		// 			setActiveStep(stepsNascimento.length - 1);
		// 			setChave(response.data.id);
		// 		}
		// 	})
		// 	.catch((err) => console.log(err));
		console.log(data);
		setActiveStep(stepsObito.length - 1);
		setChave(Guid.create().toString());
	};

	const totalSteps = () => {
		return stepsObito.length;
	};

	const completedSteps = () => {
		return Object.keys(completed).length;
	};

	const isLastStep = () => {
		return activeStep === totalSteps() - 1;
	};

	const allStepsCompleted = () => {
		return completedSteps() === totalSteps();
	};
	const onInvalid: SubmitErrorHandler<ICasamento> = (err) => {
		setToast(true);
	};

	const handleNext = () => {
		const newActiveStep =
			isLastStep() && !allStepsCompleted()
				? // It's the last step, but not all steps have been completed,
				  // find the first step that has been completed
				  stepsObito.findIndex((step, i) => !(i in completed))
				: activeStep + 1;
		setActiveStep(newActiveStep);
		if (newActiveStep === stepsObito.length - 1) alert('Enviado');
	};

	const handleBack = () => {
		if (activeStep === stepsObito.length - 1) router.push('/');
		else setActiveStep((prevActiveStep) => prevActiveStep - 1);
	};

	const isActive = (route: string) => {
		return !(stepsObito.find((s, i) => i === activeStep)?.route === route);
	};
	const disabledSubmit =
		!formState.isValid && !(activeStep === stepsObito.length - 1);

	const buildButtons = () => {
		return (
			isDesktop() && (
				<Grid
					container
					direction={'row'}
					spacing={2}
					style={{ padding: '2rem 2rem' }}
				>
					<Grid item>
						<Button
							disabled={
								activeStep === 0 || activeStep === stepsObito.length - 1
							}
							color="primary"
							variant="contained"
							onClick={handleBack}
						>
							<NavigateBeforeIcon />
						</Button>
					</Grid>
					<Grid item>
						<Button
							variant="contained"
							color="primary"
							onClick={handleNext}
							disabled={disabledNext()}
						>
							<NavigateNextIcon />
						</Button>
					</Grid>
				</Grid>
			)
		);
	};
	const disabledNext = () =>
		activeStep === totalSteps() - 1 || activeStep === totalSteps() - 2;

	return (
		<Container>
			<Steppers
				formSteps={stepsObito}
				step={activeStep}
				nextDisabled={disabledNext()}
				next={handleNext}
				prev={handleBack}
			/>
			<div style={{ marginTop: 10 }}>
				<form>
					<Grid
						spacing={2}
						container
						direction="column"
						alignItems="stretch"
						style={{ marginBottom: '1vh' }}
					>
						{buildButtons()}
						<Grid item>
							<RequerenteForm
								errors={errors}
								control={control}
								active={isActive}
								current={activeStep}
								watch={watch}
							/>
							<RegistradoForm
								errors={errors}
								control={control}
								active={isActive}
								current={activeStep}
								watch={watch}
								reset={reset}
							/>
							<FalecimentoForm
								errors={errors}
								control={control}
								active={isActive}
								current={activeStep}
								watch={watch}
							/>
							<FiliacaoForm
								errors={errors}
								control={control}
								active={isActive}
								current={activeStep}
								watch={watch}
								setValue={setValue}
							/>
							<AvosForm
								errors={errors}
								control={control}
								active={isActive}
								current={activeStep}
								watch={watch}
							/>
							<ResultForm
								chave={chave}
								active={isActive}
								current={activeStep}
							/>
						</Grid>
						{activeStep === stepsObito.length - 2 && (
							<Grid item style={{ height: '10vh', position: 'relative' }}>
								<Fab
									color="primary"
									aria-label="salvar"
									style={{ position: 'absolute', right: 30 }}
									onClick={handleSubmit(onSubmit)}
								>
									<SaveIcon color={'action'} />
								</Fab>
							</Grid>
						)}
					</Grid>
				</form>
			</div>
		</Container>
	);
};

WizzardPageObito.layout = ExternalLayout;
export default WizzardPageObito;
