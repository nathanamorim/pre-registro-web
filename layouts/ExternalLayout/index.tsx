import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import clsx from 'clsx';
import Image from 'next/image';
import React from 'react';
import { LayoutProps } from '../layoutsTypes';
import logo from '../../public/assets/img/logo_registro_civil.svg';
import externalLayout from './styles';
import StickyFooter from '../../components/StickyFooter';
import IconButton from '@material-ui/core/IconButton';
import AppsIcon from '@material-ui/icons/Apps';
import { useRouter } from 'next/router';
import { Grid } from '@material-ui/core';

const ExternalLayout = (layout: LayoutProps) => {
	const classes = externalLayout();
	const [open, setOpen] = React.useState(false);

	const handleDrawerOpen = () => {
		setOpen(true);
	};

	const handleDrawerClose = () => {
		setOpen(false);
	};
	const handleLink = () => {
		window.location.href = 'https://registrocivil.org.br/';
	};
	const route = useRouter();
	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar position="fixed" color={'default'} elevation={0}>
				<Toolbar>
					<Grid container direction="row" alignItems="center">
						<Grid item xs={6} md={10}>
							<IconButton
								color="default"
								aria-label="upload picture"
								component="span"
								style={{ marginRight: 10 }}
								onClick={() => route.push('/')}
							>
								<AppsIcon />
							</IconButton>
						</Grid>
						<Grid item xs={6} md={2}>
							<div className={classes.logo} onClick={handleLink}>
								<Image
									src={logo}
									width={'120px'}
									alt="Registro Civil de São Paulo"
								/>
							</div>
						</Grid>
					</Grid>
				</Toolbar>
			</AppBar>
			<main
				className={clsx(classes.content, {
					[classes.contentShift]: open,
				})}
			>
				{layout.children}
			</main>
			<StickyFooter />
		</div>
	);
};

export default ExternalLayout;
