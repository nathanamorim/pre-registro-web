import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import React from 'react';
import Sidebar from '../../components/Sidebar';
import { LayoutProps } from '../layoutsTypes';
import internalStyles from './styles';

const InternalLayout = (layout: LayoutProps) => {
	const classes = internalStyles();
	const [open, setOpen] = React.useState(false);

	const handleDrawerOpen = () => {
		setOpen(true);
	};

	const handleDrawerClose = () => {
		setOpen(false);
	};

	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar
				color={'default'}
				position="fixed"
				className={clsx(classes.appBar, {
					[classes.appBarShift]: open,
				})}
			>
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						onClick={handleDrawerOpen}
						edge="start"
						className={clsx(classes.menuButton, open && classes.hide)}
					>
						<MenuIcon />
					</IconButton>
					<Typography variant="h6" noWrap>
						{process.env.appName}
					</Typography>
				</Toolbar>
			</AppBar>
			<Sidebar open={open} handleClose={handleDrawerClose} />
			<main
				className={clsx(classes.content, {
					[classes.contentShift]: open,
				})}
			>
				{layout.children}
			</main>
		</div>
	);
};

export default InternalLayout;
