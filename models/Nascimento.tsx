import {
	IRequerente,
	IRegistrado,
	INascimento,
	IFiliacao,
	IAvos,
} from '../interfaces/Nascimento';
import moment from 'moment';
export class NascimentoDto {
	requerente?: IRequerente;
	registrado?: IRegistrado;
	filiacao?: IFiliacao;
	avos?: IAvos;
	constructor(public dto: INascimento) {}
}

class Requerente {
	/**
	 *
	 */
	constructor() {}
}
