import { createStyles, makeStyles, Theme } from '@material-ui/core';

const drawerWidth = 240;
const sidebarStyle = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
		},
		drawer: {
			width: drawerWidth,
			flexShrink: 0,
		},
		drawerPaper: {
			width: drawerWidth,
		},
		drawerHeader: {
			display: 'flex',
			alignItems: 'center',
			padding: theme.spacing(0, 1),
			// necessary for content to be below app bar
			...theme.mixins.toolbar,
			justifyContent: 'flex-end',
		},
		logo: {
			cursor: 'pointer',
		},
	})
);
export default sidebarStyle;
