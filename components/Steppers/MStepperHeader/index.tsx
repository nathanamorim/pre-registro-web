import React, { Fragment } from 'react';
import { Grid } from '@material-ui/core';
import StepLabel from '@material-ui/core/StepLabel';
import Step from '@material-ui/core/Step';
import Stepper from '@material-ui/core/Stepper';
import { isDesktop } from '../../../shared/isDesktop';
type IMStepperHeader = {
	label: string;
	step: number;
};

const MStepperHeader = ({ label, step }: IMStepperHeader) => {
	return !isDesktop() ? (
		<Grid item xs={12} md={6}>
			<Stepper
				alternativeLabel
				activeStep={step}
				style={{ background: '#fafafa', marginBottom: 20 }}
			>
				<Step>
					<StepLabel>{label}</StepLabel>
				</Step>
			</Stepper>
		</Grid>
	) : (
		<Fragment />
	);
};

export default MStepperHeader;
