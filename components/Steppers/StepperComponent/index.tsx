import StepLabel from '@material-ui/core/StepLabel';
import Step from '@material-ui/core/Step';
import Stepper from '@material-ui/core/Stepper';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import React from 'react';
import { IStepperComponent } from '../../../interfaces/IStepperComponent';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			width: '100%',
		},
		button: {
			marginRight: theme.spacing(1),
		},
		instructions: {
			marginTop: theme.spacing(1),
			marginBottom: theme.spacing(1),
		},
		content: {
			padding: theme.spacing(0, 10),
		},
	})
);

const StepperComponent = (props: IStepperComponent) => {
	const { formSteps, step } = props;
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Stepper
				alternativeLabel
				activeStep={step}
				style={{ background: '#fafafa', marginBottom: 20 }}
			>
				{formSteps?.map((form, index) => (
					<Step key={form.label}>
						<StepLabel>{form.label}</StepLabel>
					</Step>
				))}
			</Stepper>
		</div>
	);
};

export default StepperComponent;
