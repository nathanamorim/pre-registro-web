import React from 'react';
import { IMStepperComponent } from '../../interfaces/IStepperComponent';
import { isDesktop } from '../../shared/isDesktop';
import StepperComponent from './StepperComponent';
import MStepperComponent from './MStepperComponent';

const Steppers = ({
	step,
	formSteps,
	next,
	nextDisabled,
	prev,
}: IMStepperComponent) => {
	return isDesktop() ? (
		<StepperComponent formSteps={formSteps} step={step} />
	) : (
		<MStepperComponent
			formSteps={formSteps}
			step={step}
			nextDisabled={nextDisabled}
			next={next}
			prev={prev}
		/>
	);
};

export default Steppers;
