import React from 'react';
import {
	createStyles,
	makeStyles,
	Theme,
	useTheme,
} from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { Grid } from '@material-ui/core';
import { IMStepperComponent } from '../../../interfaces/IStepperComponent';

const styles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			maxWidth: 400,
			flexGrow: 1,
		},
		button: {
			marginRight: theme.spacing(1),
		},
	})
);

function getSteps() {
	return ['Requerente', 'Registrado', 'Filiação', 'Avós', 'Finalizado'];
}

const MStepperComponent = (props: IMStepperComponent) => {
	const { formSteps, step, next, prev, nextDisabled } = props;
	const classes = styles();
	const theme = useTheme();

	return (
		<Grid item xs>
			<MobileStepper
				variant="progress"
				steps={formSteps.length}
				position="bottom"
				activeStep={step}
				className={classes.root}
				nextButton={
					<Button
						size="small"
						onClick={next}
						disabled={step === formSteps.length || nextDisabled}
					>
						Próximo
						{theme.direction === 'rtl' ? (
							<KeyboardArrowLeft />
						) : (
							<KeyboardArrowRight />
						)}
					</Button>
				}
				backButton={
					<Button size="small" onClick={prev} disabled={step === 0}>
						{theme.direction === 'rtl' ? (
							<KeyboardArrowRight />
						) : (
							<KeyboardArrowLeft />
						)}
						Voltar
					</Button>
				}
			/>
		</Grid>
	);
};

export default MStepperComponent;
