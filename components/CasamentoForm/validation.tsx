import * as validation from 'yup';
import { ptShort } from 'yup-locale-pt';

validation.setLocale(ptShort);
export const CasamentoValidation = validation.object().shape({
	pretendentes: validation.array().of(
		validation.object().shape({
			cpf: validation.string().required(),
			nome: validation.string().required(),
			email: validation.string().email().required(),
		})
	),
	testemunhas: validation.array().of(
		validation.object().shape({
			name: validation.string().required(),
			email: validation.string().email().required(),
		})
	),
	filiacao: validation.object().shape({
		mae: validation.object().shape({
			nome: validation.string().required().min(3),
			cpf: validation.string().required().min(11),
		}),
	}),
});
