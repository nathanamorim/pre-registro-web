import { Container, Grid, TextField, Typography } from '@material-ui/core';
import React, { Fragment } from 'react';
import { Controller } from 'react-hook-form';
import { IForm, IDynamicFormItem } from '../../interfaces/IForm';
import InputCPFMask from '../InputCPFMask';
import InputTelefoneMask from '../InputTelefoneMask';
import MStepperHeader from '../Steppers/MStepperHeader';
import OneAccordion from '../OneAccordion';
import { Data } from '../../shared/utils';

const TestemunhasForm = ({
	active,
	control,
	errors,
	current,
	watch,
}: IForm) => {
	return !active('testemunhas') ? (
		<Container style={{ marginBottom: '2rem' }}>
			<MStepperHeader label={'Testemunhas'} step={current} />
			{[1, 2].map((i) => (
				<OneAccordion label={`Testemunha ${i}`} open={true} key={i}>
					<TestemunhaItemForm
						errors={errors}
						control={control}
						watch={watch}
						id={i}
					/>
				</OneAccordion>
			))}
		</Container>
	) : (
		<Fragment />
	);
};

export default TestemunhasForm;

const TestemunhaItemForm = ({
	control,
	errors,
	watch,
	id,
}: IDynamicFormItem) => {
	return (
		<Fragment>
			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].cpf`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.cpf}
							helperText={errors?.testemunhas?.[id]?.cpf?.message}
							label="CPF"
							id={`testemunhas-${id}-cpf`}
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputCPFMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].rg`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.rg}
							helperText={errors?.testemunhas?.[id]?.rg?.message}
							label="RG"
							id={`testemunhas-${id}-rg`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name={`testemunhas[${id}].nome`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.nome}
							helperText={errors?.testemunhas?.[id]?.nome?.message}
							label="Nome Completo"
							id={`testemunhas-[${id}]nome`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].telefone`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.telefone}
							helperText={errors?.testemunhas?.[id]?.telefone?.message}
							label="Telefone"
							id={`testemunhas-[${id}]-telefone`}
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputTelefoneMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>

			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].email`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.email}
							helperText={errors?.testemunhas?.[id]?.email?.message}
							label="E-mail"
							id={`testemunhas-${id}-email`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name={`testemunhas[${id}].naturalidadeUf`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.naturalidadeUf}
							helperText={errors?.testemunhas?.[id]?.naturalidadeUf?.message}
							label="UF Naturalidade"
							id={`testemunhas-${id}-naturalidadeUf`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name={`testemunhas[${id}].naturalidade`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.naturalidade}
							helperText={errors?.testemunhas?.[id]?.naturalidade?.message}
							label="Naturalidade"
							id={`testemunhas-${id}naturalidade`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].nacionalidade`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.nacionalidade}
							helperText={errors?.testemunhas?.[id]?.nacionalidade?.message}
							label="Nacionalidade"
							id={`testemunhas-${id}-nacionalidade`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>

			<Grid item xs={12} md={3}>
				<Controller
					name={`testemunhas[${id}].dataNascimento`}
					control={control}
					render={({ field }) => (
						<TextField
							id={`testemunhas-${id}-data-nascimento`}
							label="Data de nascimento"
							type="date"
							defaultValue={new Data().now('yyyy-MM-DD')}
							{...field}
							error={!!errors.testemunhas?.[id]?.dataNascimento}
							helperText={errors?.testemunhas?.[id]?.dataNascimento?.message}
							variant="outlined"
							fullWidth
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={9}>
				<Controller
					name={`testemunhas[${id}].profissao`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.profissao}
							helperText={errors?.testemunhas?.[id]?.profissao?.message}
							label="Profissão"
							id={`testemunhas-${id}-profissao`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name="testemunhas.endereco"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.endereco}
							helperText={errors?.testemunhas?.[id]?.endereco?.message}
							label="Endereco"
							id={`testemunhas-${id}-endereco`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
		</Fragment>
	);
};
