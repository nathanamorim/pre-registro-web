import React, { Fragment } from 'react';
import {
	Container,
	FormControl,
	FormControlLabel,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	Switch,
	TextField,
} from '@material-ui/core';
import { Controller } from 'react-hook-form';
import { IForm } from '../../interfaces/IForm';
import MStepperHeader from '../Steppers/MStepperHeader';
import { ISelect } from '../../interfaces/ISelect';
import {
	EnumNaturalidadeNascimento,
	EnumTipoLocalNascimento,
} from '../../shared/enum';
import { Data } from '../../shared/utils';

enum EnumLocalCasamento {
	cartorio = 'cartorio',
	domicilio = 'domicilio',
	religioso = 'religioso',
}
const LocalCasamento: ISelect[] = [
	{
		label: 'Cartório de Registro Civil',
		value: EnumLocalCasamento.cartorio,
	},
	{
		label: 'Em domicílio',
		value: EnumLocalCasamento.domicilio,
	},
	{
		label: 'Religioso',
		value: EnumLocalCasamento.religioso,
	},
];

const RegimeDeBens: ISelect[] = [
	{ label: 'Separação de bens', value: 'separacao' },
	{ label: 'Comunhão parcial', value: 'comunhao-parcial' },
	{
		label: 'Comunhão universal',
		value: 'comunhao-universal',
	},
	{ label: 'Participação final dos aquestos', value: 'final-aquestos' },
];

const CasamentoForm = ({ active, control, errors, current, watch }: IForm) => {
	const watchLocal = watch('localCasamento.local');
	return !active('casamento') ? (
		<Container style={{ marginBottom: '2rem' }}>
			<MStepperHeader label={'Requerente'} step={current} />
			<Grid container spacing={4}>
				<Grid item xs={12} md={4}>
					<Controller
						name="localCasamento.local"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.localCasamento?.local}
							>
								<InputLabel>Local da cerimônia do casamento</InputLabel>
								<Select id="local-casamento-local" {...field}>
									{LocalCasamento.map((tipo) => (
										<MenuItem key={tipo.value} value={tipo.value}>
											{tipo.label}
										</MenuItem>
									))}
								</Select>
								<FormHelperText>
									{!!errors.registrado?.naturalidade &&
										errors?.registrado?.naturalidade?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>

				<Grid item xs={12} md={4}>
					<Controller
						name="localCasamento.regimeBens"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.localCasamento?.regimeBens}
							>
								<InputLabel>Regime de bens</InputLabel>
								<Select id="casamento-regime-bens" {...field}>
									{RegimeDeBens.map((tipo) => (
										<MenuItem key={tipo.value} value={tipo.value}>
											{tipo.label}
										</MenuItem>
									))}
								</Select>
								<FormHelperText>
									{!!errors.localCasamento?.regimeBens &&
										errors?.localCasamento?.regimeBens?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="localCasamento.dataCasamento"
						control={control}
						render={({ field: { onChange, value, ref } }) => (
							<TextField
								id="registrado-data-Casamento"
								label="Data e Hora de Casamento"
								type="datetime-local"
								defaultValue={new Data().now('yyyy-MM-DDTHH:mm')}
								onChange={onChange}
								value={value}
								inputRef={ref}
								error={!!errors.localCasamento?.dataCasamento}
								helperText={errors?.localCasamento?.dataCasamento?.message}
								variant="outlined"
								fullWidth
							/>
						)}
					/>
				</Grid>
				{watchLocal === 'religioso' && (
					<Fragment>
						<Grid item xs={12} md={4}>
							<Controller
								name="registrado.nome"
								control={control}
								render={({ field }) => (
									<TextField
										error={!!errors.registrado?.nome}
										helperText={errors?.registrado?.nome?.message}
										label="Nome da Igreja"
										id="registrado-nome"
										variant="outlined"
										fullWidth
										{...field}
									/>
								)}
							/>
						</Grid>
						<Grid item xs={12} md={8}>
							<Controller
								name="localCasamento.enderecoIgreja"
								control={control}
								render={({ field }) => (
									<TextField
										error={!!errors.localCasamento?.enderecoIgreja}
										helperText={errors?.localCasamento?.enderecoIgreja?.message}
										label="Endereço da igreja"
										id="localCasamento-enderecoIgreja"
										variant="outlined"
										fullWidth
										{...field}
									/>
								)}
							/>
						</Grid>
					</Fragment>
				)}
			</Grid>
		</Container>
	) : (
		<Fragment />
	);
};

export default CasamentoForm;
