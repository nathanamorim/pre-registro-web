import React, { Fragment } from 'react';
import {
	Container,
	FormControl,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
} from '@material-ui/core';
import { Controller } from 'react-hook-form';
import { IDynamicFormItem, IForm, IFormItem } from '../../interfaces/IForm';
import MStepperHeader from '../Steppers/MStepperHeader';
import InputCPFMask from '../InputCPFMask';
import InputTelefoneMask from '../InputTelefoneMask';
import OneAccordion from '../OneAccordion';
import { Data } from '../../shared/utils';

const PretendentesForm = ({
	errors,
	active,
	control,
	current,
	watch,
}: IForm) => {
	return !active('pretendentes') ? (
		<Container>
			<MStepperHeader label={'Pretendentes'} step={current} />
			{[1, 2].map((i) => (
				<OneAccordion label={`Pretendente ${i}`} open={true} key={i}>
					<PretendenteItemForm
						errors={errors}
						control={control}
						watch={watch}
						id={i}
					/>
				</OneAccordion>
			))}
		</Container>
	) : (
		<Fragment />
	);
};

export default PretendentesForm;

const PretendenteOne = ({ control, errors, watch }: IFormItem) => {
	return (
		<Fragment>
			<Grid item xs={12} md={4}>
				<Controller
					name="requerente.parentesco"
					control={control}
					render={({ field }) => (
						<FormControl
							variant="outlined"
							fullWidth
							error={!!errors.requerente?.parentesco}
						>
							<InputLabel>Grau de parentesco</InputLabel>
							<Select
								id="requerente-parentesco"
								{...field}
								defaultValue={''}
								label="Sexo"
							>
								<MenuItem value={'P'}>Pai</MenuItem>
								<MenuItem value={'M'}>Mãe</MenuItem>
								<MenuItem value={'O'}>Outro</MenuItem>
							</Select>
							<FormHelperText>
								{!!errors.requerente?.parentesco &&
									errors?.requerente?.parentesco?.message}
							</FormHelperText>
						</FormControl>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={8}>
				<Controller
					name="requerente.nome"
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.requerente?.nome}
							helperText={errors?.requerente?.nome?.message}
							label="Nome Completo"
							id="requerente-nome"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md>
				<Controller
					name="requerente.cpf"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.requerente?.cpf}
							helperText={errors?.requerente?.cpf?.message}
							label="CPF"
							id="requerente-cpf"
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputCPFMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name="requerente.telefone"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.requerente?.telefone}
							helperText={errors?.requerente?.telefone?.message}
							label="Telefone"
							id="requerente-telefone"
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputTelefoneMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name="requerente.email"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.requerente?.email}
							helperText={errors?.requerente?.email?.message}
							label="E-mail"
							id="requerente-email"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
		</Fragment>
	);
};

const PretendenteItemForm = ({
	control,
	errors,
	watch,
	id,
}: IDynamicFormItem) => {
	return (
		<Fragment>
			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].cpf`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.cpf}
							helperText={errors?.testemunhas?.[id]?.cpf?.message}
							label="CPF"
							id={`testemunhas-${id}-cpf`}
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputCPFMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].rg`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.testemunhas?.[id]?.rg}
							helperText={errors?.testemunhas?.[id]?.rg?.message}
							label="RG"
							id={`testemunhas-${id}-rg`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name={`testemunhas[${id}].nome`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.nome}
							helperText={errors?.filiacao?.[id]?.nome?.message}
							label="Nome Completo"
							id={`testemunhas-[${id}]nome`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].telefone`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.telefone}
							helperText={errors?.filiacao?.[id]?.telefone?.message}
							label="Telefone"
							id={`testemunhas-[${id}]-telefone`}
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputTelefoneMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>

			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].email`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.email}
							helperText={errors?.filiacao?.[id]?.email?.message}
							label="E-mail"
							id={`testemunhas-${id}-email`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name={`testemunhas[${id}].naturalidadeUf`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.naturalidadeUf}
							helperText={errors?.filiacao?.[id]?.naturalidadeUf?.message}
							label="UF Naturalidade"
							id={`testemunhas-${id}-naturalidadeUf`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name={`testemunhas[${id}].naturalidade`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.naturalidade}
							helperText={errors?.filiacao?.[id]?.naturalidade?.message}
							label="Naturalidade"
							id={`testemunhas-${id}naturalidade`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`testemunhas[${id}].nacionalidade`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.nacionalidade}
							helperText={errors?.filiacao?.[id]?.nacionalidade?.message}
							label="Nacionalidade"
							id={`testemunhas-${id}-nacionalidade`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>

			<Grid item xs={12} md={3}>
				<Controller
					name={`testemunhas[${id}].dataNascimento`}
					control={control}
					render={({ field }) => (
						<TextField
							id={`testemunhas-${id}-data-nascimento`}
							label="Data de nascimento"
							type="date"
							defaultValue={new Data().now('yyyy-MM-DD')}
							{...field}
							error={!!errors.filiacao?.[id]?.dataNascimento}
							helperText={errors?.filiacao?.[id]?.dataNascimento?.message}
							variant="outlined"
							fullWidth
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={9}>
				<Controller
					name={`testemunhas[${id}].profissao`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.profissao}
							helperText={errors?.filiacao?.[id]?.profissao?.message}
							label="Profissão"
							id={`testemunhas-${id}-profissao`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name="testemunhas.endereco"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.endereco}
							helperText={errors?.filiacao?.[id]?.endereco?.message}
							label="Endereco"
							id={`testemunhas-${id}-endereco`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
		</Fragment>
	);
};
