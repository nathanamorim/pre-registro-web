import React, { Fragment } from 'react';
import {
	Container,
	FormControl,
	FormControlLabel,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	Switch,
	TextField,
	Typography,
} from '@material-ui/core';
import { Controller } from 'react-hook-form';
import { IForm } from '../../interfaces/IForm';
import MStepperHeader from '../Steppers/MStepperHeader';
import { ISelect } from '../../interfaces/ISelect';
import {
	EnumNaturalidadeNascimento,
	EnumTipoLocalNascimento,
} from '../../shared/enum';

const baseSelect: ISelect[] = [
	{
		label: 'Opção 1',
		value: 1,
	},
	{
		label: 'Opção 2',
		value: 2,
	},
];

const route = 'base';

const RegistradoForm = ({ active, control, errors, current, watch }: IForm) => {
	return !active(route) ? (
		<Container style={{ marginBottom: '2rem' }}>
			<MStepperHeader label={'Requerente'} step={current} />
			<Grid container spacing={4}>
				<Grid item xs={12} md={4}>
					<Controller
						name="tipo.campo"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.tipo?.campo}
								helperText={errors?.tipo?.campo?.message}
								label="Tipo Campo"
								id="tipo-campo"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="tipo.sexo"
						control={control}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.tipo?.sexo}
							>
								<InputLabel>Sexo</InputLabel>
								<Select
									id="tipo-sexo"
									{...field}
									defaultValue={''}
									label="Sexo"
								>
									<MenuItem value={'M'}>Masculino</MenuItem>
									<MenuItem value={'F'}>Feminino</MenuItem>
								</Select>
								<FormHelperText>
									{!!errors.tipo?.sexo && errors?.tipo?.sexo?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="tipo.switch"
						control={control}
						render={({ field: { onChange, value } }) => (
							<FormControlLabel
								control={
									<Switch color="primary" checked={value} onChange={onChange} />
								}
								label="Tipo Switch"
							/>
						)}
					/>
				</Grid>
			</Grid>
		</Container>
	) : (
		<Fragment />
	);
};

export default RegistradoForm;
