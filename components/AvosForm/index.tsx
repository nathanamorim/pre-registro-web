import { Container, Grid, TextField, Typography } from '@material-ui/core';
import React, { Fragment } from 'react';
import { Controller } from 'react-hook-form';
import { IForm } from '../../interfaces/IForm';
import OneAccordion from '../OneAccordion';
import MStepperHeader from '../Steppers/MStepperHeader';

const AvosForm = ({ active, control, errors, current, watch }: IForm) => {
	return !active('avos') ? (
		<Container style={{ marginBottom: '2rem' }}>
			<MStepperHeader label={'Avós'} step={current} />
			<OneAccordion label={'Dados Maternos'} open={true}>
				<Grid item xs={12} md={12}>
					<Controller
						name="avos.maternos.avoM"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.avos?.maternos?.avoM}
								helperText={errors?.avos?.maternos?.avoM.message}
								label="Nome da avó"
								id="avos-maternos-avo-M"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={12}>
					<Controller
						name="avos.maternos.avo"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.avos?.maternos?.avo}
								helperText={errors?.avos?.maternos?.avo.message}
								label="Nome do avô"
								id="avos-maternos-avo"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
			</OneAccordion>
			<OneAccordion label={'Dados Paternos'} open={false}>
				<Grid item xs={12} md={12}>
					<Controller
						name="avos.paternos.avoM"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.avos?.paternos?.avoM}
								helperText={errors?.avos?.paternos?.avoM.message}
								label="Nome da avó"
								id="avos-paternos-avo-M"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={12}>
					<Controller
						name="avos.paternos.avo"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.avos?.paternos?.avo}
								helperText={errors?.avos?.paternos?.avo.message}
								label="Nome do avô"
								id="avos-paternos-avo"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
			</OneAccordion>
		</Container>
	) : (
		<Fragment />
	);
};

export default AvosForm;
