import {
	FormControl,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import React, { Fragment } from 'react';
import { Controller } from 'react-hook-form';
import { IFormFieldsWithLabel } from '../../interfaces/IForm';

type AvosForm = {
	sexo: string;
} & IFormFieldsWithLabel;

const AvoFields = ({ control, errors, id, node, label, sexo }: AvosForm) => {
	return id ? (
		<Fragment>
			<Grid item xs={12} md={9}>
				<Controller
					name={`${node}.avos[${id}].nome`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors?.[node]?.avos?.[id]?.nome}
							helperText={errors?.[node]?.avos?.[id]?.nome?.message}
							label={label}
							id={`${node}-avos-${id}-nome`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={3}>
				<Controller
					name={`${node}.avos[${id}].sexo`}
					control={control}
					render={({ field }) => (
						<FormControl
							variant="outlined"
							fullWidth
							error={!!errors.registrado?.sexo}
						>
							<InputLabel id={`avos-${id}-sexo`}>Sexo</InputLabel>
							<Select
								id={`avos-${id}-sexo`}
								{...field}
								defaultValue={sexo}
								label="Sexo"
							>
								<MenuItem value={'M'}>Masculino</MenuItem>
								<MenuItem value={'F'}>Feminino</MenuItem>
							</Select>
							<FormHelperText>
								{!!errors.avos?.[id]?.sexo && errors?.avos?.[id]?.sexo?.message}
							</FormHelperText>
						</FormControl>
					)}
				/>
			</Grid>
		</Fragment>
	) : (
		<Fragment />
	);
};

export default AvoFields;
