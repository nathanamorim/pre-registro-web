import React, { PropsWithChildren } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, { AlertProps, Color } from '@material-ui/lab/Alert';
import { makeStyles, Theme } from '@material-ui/core/styles';

function Alert(props: AlertProps) {
	return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		width: '100%',
		'& > * + *': {
			marginTop: theme.spacing(2),
		},
	},
}));
type IToast = {
	duration: number;
	show: boolean;
	type: Color;
};
export default function ToastComponent({
	children,
	show,
	type,
	duration,
}: PropsWithChildren<IToast>) {
	const classes = useStyles();
	const [open, setOpen] = React.useState(show);

	const handleClick = () => {
		setOpen(true);
	};

	const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
		if (reason === 'clickaway') {
			return;
		}

		setOpen(!show);
	};

	return (
		<div className={classes.root}>
			<Snackbar open={open} autoHideDuration={duration} onClose={handleClose}>
				<Alert onClose={handleClose} severity={type}>
					{children}
				</Alert>
			</Snackbar>
		</div>
	);
}
