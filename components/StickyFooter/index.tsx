import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import logoArpen from '../../public/assets/img/arpen-br.svg';
import logoCnj from '../../public/assets/img/cnj-white.svg';
import { Grid, useMediaQuery } from '@material-ui/core';
import Image from 'next/image';

const useStyles = makeStyles((theme) => ({
	main: {
		marginTop: theme.spacing(8),
		marginBottom: theme.spacing(2),
	},
	footer: {
		padding: theme.spacing(3, 2),
		marginTop: 'auto',
		backgroundColor: '#6c757d',
		maxHeight: '150px',
	},
	logo: {
		cursor: 'pointer',
		maxHeight: 80,
	},
	colBorder: {
		borderRight: '1px solid #FFF',
	},
}));

const StickyFooter = () => {
	const classes = useStyles();
	const isDesktop = useMediaQuery('(min-width:600px)');

	const handleCnjLink = () => {
		window.location.href = 'https://atos.cnj.jus.br/atos/detalhar/2509';
	};

	const handleArpenLink = () => {
		window.location.href = 'https://arpenbrasil.org.br/';
	};

	return isDesktop ? (
		<footer className={classes.footer} color={''}>
			<Grid
				container
				spacing={2}
				direction="row"
				alignItems="center"
				justify="flex-end"
			>
				<Grid item xs={6} md={2} className={classes.colBorder}>
					<Image
						onClick={handleCnjLink}
						className={classes.logo}
						src={logoArpen}
						alt={
							'Associação dos Registradores de Pessoas Naturais do Estado de São Paulo'
						}
						layout={'responsive'}
						height={80}
					/>
				</Grid>
				<Grid item xs={6} md={1}>
					<Image
						onClick={handleArpenLink}
						className={classes.logo}
						src={logoCnj}
						alt={'Conselho Nacional de Justiça'}
						layout={'responsive'}
						height={80}
					/>
				</Grid>
			</Grid>
		</footer>
	) : (
		<></>
	);
};

export default StickyFooter;
