import { Container, Grid, TextField } from '@material-ui/core';
import React, { Fragment } from 'react';
import { Controller } from 'react-hook-form';
import {
	IFormItemWithSetValue,
	IFormWithSetValue,
} from '../../interfaces/IForm';
import InputCPFMask from '../InputCPFMask';
import InputTelefoneMask from '../InputTelefoneMask';
import MStepperHeader from '../Steppers/MStepperHeader';
import OneAccordion from '../OneAccordion';
import EnderecoFields from '../EnderecoFields';
import { Data } from '../../shared/utils';
import { FiliacaoFields } from './item';

const FiliacaoForm = ({
	active,
	control,
	errors,
	current,
	watch,
	setValue,
}: IFormWithSetValue) => {
	return !active('filiacao') ? (
		<Container style={{ marginBottom: '2rem' }}>
			<MStepperHeader label={'Filiação'} step={current} />
			{[1, 2].map((i) => (
				<OneAccordion label={`Filiação ${i}`} open={true} key={i}>
					<FiliacaoFields
						errors={errors}
						control={control}
						watch={watch}
						setValue={setValue}
						id={i}
					/>
				</OneAccordion>
			))}
		</Container>
	) : (
		<Fragment />
	);
};

export default FiliacaoForm;

export const FiliacaoFormMae = ({
	control,
	errors,
	watch,
	setValue,
	id,
}: IFormItemWithSetValue) => {
	const [key, setKey] = React.useState(0);
	React.useEffect(() => {
		if (id) setKey(id);
	}, [id]);
	return (
		<Fragment>
			<Grid item xs={12} md={12}>
				<Controller
					name={`filiacao[${key}].cpf`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[key]?.cpf}
							helperText={errors?.filiacao?.[key]?.cpf?.message}
							label="CPF"
							id={`filiacao-${key}-cpf`}
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputCPFMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`filiacao[${key}].rg.numero`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[key]?.rg}
							helperText={errors?.filiacao?.[key]?.rg?.message}
							label="RG"
							id={`filiacao-${key}-rg-numero`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name={`filiacao[${key}].rg.orgao`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[key]?.rg?.orgao}
							helperText={errors?.filiacao?.[key]?.rg?.orgao?.message}
							label="Orgão Expedidor"
							id={`filiacao-${key}-orgao-expedidor`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name={`filiacao[${key}].rg.dataExpedicao`}
					control={control}
					render={({ field }) => (
						<TextField
							id={`filiacao-${key}-rg-dataExpedicao`}
							label="Data de Expedição"
							type="date"
							defaultValue={new Data().now('yyyy-MM-DD')}
							{...field}
							error={!!errors.filiacao?.[key]?.dataExpedicao}
							helperText={errors?.filiacao?.[key]?.dataExpedicao?.message}
							variant="outlined"
							fullWidth
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name={`filiacao[${key}].nome`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[key]?.nome}
							helperText={errors?.filiacao?.[key]?.nome?.message}
							label="Nome Completo"
							id={`filiacao-${key}-nome`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`filiacao[${key}].telefone`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[key]?.telefone}
							helperText={errors?.filiacao?.[key]?.telefone?.message}
							label="Telefone"
							id={`filiacao-${key}-telefone`}
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputTelefoneMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>

			<Grid item xs={12} md={6}>
				<Controller
					name={`filiacao[${key}].email`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[key]?.email}
							helperText={errors?.filiacao?.[key]?.email?.message}
							label="E-mail"
							id={`filiacao-${key}-email`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name={`filiacao[${key}].naturalidade.uf`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[key]?.naturalidade?.uf}
							helperText={errors?.filiacao?.[key]?.naturalidade?.uf?.message}
							label="UF Naturalidade"
							id={`filiacao-${key}-naturalidade-uf`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name={`filiacao[${key}].naturalidade.municipio`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[key]?.naturalidade?.municipio}
							helperText={
								errors?.filiacao?.[key]?.naturalidade?.municipio?.message
							}
							label="Naturalidade"
							id={`filiacao-${key}-naturalidade-municipio`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`filiacao[${key}].nacionalidade`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[0]?.nacionalidade}
							helperText={errors?.filiacao?.[0]?.nacionalidade?.message}
							label="Nacionalidade"
							id={`filiacao-${key}-nacionalidade`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<EnderecoFields
				control={control}
				setValue={setValue}
				watch={watch}
				errors={errors}
				node={`filiacao[${key}]`}
			/>
			<Grid item xs={12} md={3}>
				<Controller
					name={`filiacao[${key}].dataNascimento`}
					control={control}
					render={({ field }) => (
						<TextField
							id={`filiacao-${key}-dataNascimento`}
							label="Data de nascimento"
							defaultValue={new Data().now('1930-01-01')}
							type="date"
							{...field}
							error={!!errors.filiacao?.[key]?.dataNascimento}
							helperText={errors?.filiacao?.[key]?.dataNascimento?.message}
							variant="outlined"
							fullWidth
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={9}>
				<Controller
					name={`filiacao[${key}].profissao`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[key]?.profissao}
							helperText={errors?.filiacao?.[key]?.profissao?.message}
							label="Profissão"
							id={`filiacao-${key}-profissao`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
		</Fragment>
	);
};

const FiliacaoFormPai = ({
	control,
	errors,
	watch,
	setValue,
	id,
}: IFormItemWithSetValue) => {
	return (
		<Fragment>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.cpf"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.cpf}
							helperText={errors?.filiacao?.pai?.cpf?.message}
							label="CPF"
							id="filiacao-cpf"
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputCPFMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.rg"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.rg}
							helperText={errors?.filiacao?.pai?.rg?.message}
							label="RG"
							id="filiacao-rg"
							variant="outlined"
							placeholder="___.___.___-__"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name="filiacao.pai.nome"
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.nome}
							helperText={errors?.filiacao?.pai?.nome?.message}
							label="Nome Completo"
							id="filiacao-pai-nome"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.telefone"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.telefone}
							helperText={errors?.filiacao?.pai?.telefone?.message}
							label="Telefone"
							id="filiacao-pai-telefone"
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputTelefoneMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.email"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.email}
							helperText={errors?.filiacao?.pai?.email?.message}
							label="E-mail"
							id="filiacao-pai-email"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name="filiacao.pai.naturalidadeUf"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.naturalidadeUf}
							helperText={errors?.filiacao?.pai?.naturalidadeUf?.message}
							label="UF Naturalidade"
							id="filiacao-pai-naturalidadeUf"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name="filiacao.pai.naturalidade"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.naturalidade}
							helperText={errors?.filiacao?.pai?.naturalidade?.message}
							label="Naturalidade"
							id="filiacao-naturalidade"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.nacionalidade"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.nacionalidade}
							helperText={errors?.filiacao?.pai?.nacionalidade?.message}
							label="Nacionalidade"
							id="filiacao-nacionalidade"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<EnderecoFields
				control={control}
				setValue={setValue}
				watch={watch}
				errors={errors}
				node={'filiacao[1]'}
			/>
			<Grid item xs={12} md={4}>
				<Controller
					name="filiacao.pai.dataNascimento"
					control={control}
					render={({ field }) => (
						<TextField
							id="filiacao-data-nascimento"
							label="Data de nascimento"
							type="date"
							{...field}
							error={!!errors.filiacao?.pai?.dataNascimento}
							helperText={errors?.filiacao?.pai?.dataNascimento?.message}
							variant="outlined"
							fullWidth
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={8}>
				<Controller
					name="filiacao.pai.profissao"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.profissao}
							helperText={errors?.filiacao?.pai?.profissao?.message}
							label="Profissão"
							id="filiacao-pai-profissao"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name="filiacao.pai.endereco"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.endereco}
							helperText={errors?.filiacao?.pai?.endereco?.message}
							label="Endereco"
							id="filiacao-endereco"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
		</Fragment>
	);
};
