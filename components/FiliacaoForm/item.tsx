import {
	FormControl,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import React, { Fragment } from 'react';
import { Controller } from 'react-hook-form';
import { IFormItemWithSetValue } from '../../interfaces/IForm';
import InputCPFMask from '../InputCPFMask';
import InputTelefoneMask from '../InputTelefoneMask';
import EnderecoFields from '../EnderecoFields';
import { Data } from '../../shared/utils';
import AvoFields from '../AvosForm/item';

export const FiliacaoFields = ({
	control,
	errors,
	watch,
	setValue,
	id,
}: IFormItemWithSetValue) => {
	return id ? (
		<Fragment>
			<Grid item xs={12} md={12}>
				<Controller
					name={`filiacao[${id}].cpf`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.cpf}
							helperText={errors?.filiacao?.[id]?.cpf?.message}
							label="CPF"
							id={`filiacao-${id}-cpf`}
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputCPFMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`filiacao[${id}].rg.numero`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.rg}
							helperText={errors?.filiacao?.[id]?.rg?.message}
							label="RG"
							id={`filiacao-${id}-rg-numero`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name={`filiacao[${id}].rg.orgao`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.rg?.orgao}
							helperText={errors?.filiacao?.[id]?.rg?.orgao?.message}
							label="Orgão Expedidor"
							id={`filiacao-${id}-orgao-expedidor`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name={`filiacao[${id}].rg.dataExpedicao`}
					control={control}
					render={({ field }) => (
						<TextField
							id={`filiacao-${id}-rg-dataExpedicao`}
							label="Data de Expedição"
							type="date"
							defaultValue={new Data().now('yyyy-MM-DD')}
							{...field}
							error={!!errors.filiacao?.[id]?.dataExpedicao}
							helperText={errors?.filiacao?.[id]?.dataExpedicao?.message}
							variant="outlined"
							fullWidth
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={8}>
				<Controller
					name={`filiacao[${id}].nome`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.nome}
							helperText={errors?.filiacao?.[id]?.nome?.message}
							label="Nome Completo"
							id={`filiacao-${id}-nome`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name={`filiacao[${id}].sexo`}
					control={control}
					render={({ field }) => (
						<FormControl
							variant="outlined"
							fullWidth
							error={!!errors.registrado?.sexo}
						>
							<InputLabel id={`filiacao-${id}-sexo`}>Sexo</InputLabel>
							<Select
								id={`filiacao-${id}-sexo`}
								{...field}
								defaultValue={''}
								label="Sexo"
							>
								<MenuItem value={'M'}>Masculino</MenuItem>
								<MenuItem value={'F'}>Feminino</MenuItem>
							</Select>
							<FormHelperText>
								{!!errors.filiacao?.[id]?.sexo &&
									errors?.filiacao?.[id]?.sexo?.message}
							</FormHelperText>
						</FormControl>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`filiacao[${id}].telefone`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.telefone}
							helperText={errors?.filiacao?.[id]?.telefone?.message}
							label="Telefone"
							id={`filiacao-${id}-telefone`}
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputTelefoneMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>

			<Grid item xs={12} md={6}>
				<Controller
					name={`filiacao[${id}].email`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.email}
							helperText={errors?.filiacao?.[id]?.email?.message}
							label="E-mail"
							id={`filiacao-${id}-email`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name={`filiacao[${id}].naturalidade.uf`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.naturalidade?.uf}
							helperText={errors?.filiacao?.[id]?.naturalidade?.uf?.message}
							label="UF Naturalidade"
							id={`filiacao-${id}-naturalidade-uf`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name={`filiacao[${id}].naturalidade.municipio`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.naturalidade?.municipio}
							helperText={
								errors?.filiacao?.[id]?.naturalidade?.municipio?.message
							}
							label="Naturalidade"
							id={`filiacao-${id}-naturalidade-municipio`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name={`filiacao[${id}].nacionalidade`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[0]?.nacionalidade}
							helperText={errors?.filiacao?.[0]?.nacionalidade?.message}
							label="Nacionalidade"
							id={`filiacao-${id}-nacionalidade`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<EnderecoFields
				control={control}
				setValue={setValue}
				watch={watch}
				errors={errors}
				node={`filiacao[${id}]`}
			/>
			<Grid item xs={12} md={3}>
				<Controller
					name={`filiacao[${id}].dataNascimento`}
					control={control}
					render={({ field }) => (
						<TextField
							id={`filiacao-${id}-dataNascimento`}
							label="Data de nascimento"
							defaultValue={new Data().now('1930-01-01')}
							type="date"
							{...field}
							error={!!errors.filiacao?.[id]?.dataNascimento}
							helperText={errors?.filiacao?.[id]?.dataNascimento?.message}
							variant="outlined"
							fullWidth
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={9}>
				<Controller
					name={`filiacao[${id}].profissao`}
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.[id]?.profissao}
							helperText={errors?.filiacao?.[id]?.profissao?.message}
							label="Profissão"
							id={`filiacao-${id}-profissao`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Typography>Dados dos Avós</Typography>
			</Grid>
			<AvoFields
				control={control}
				errors={errors}
				node={`filiacao[${id}]`}
				watch={watch}
				id={id}
				label={'Nome da avó'}
				sexo="F"
			/>
			<AvoFields
				control={control}
				errors={errors}
				node={`filiacao[${id}]`}
				watch={watch}
				label={'Nome do avô'}
				id={2}
				sexo="M"
			/>
		</Fragment>
	) : (
		<Fragment />
	);
};
export default FiliacaoFields;
