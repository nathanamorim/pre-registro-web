import { Container, Grid, Paper, Typography } from '@material-ui/core';
import React from 'react';
import { Fragment } from 'react';
import { IResultForm } from '../../interfaces/IResultForm';
import MStepperHeader from '../Steppers/MStepperHeader';

const ResultForm = ({ chave, active, current }: IResultForm) => {
	return !active('finalizado') ? (
		<Container>
			<MStepperHeader label={'Finalizado'} step={current} />

			<Grid item xs md>
				<Typography variant="h5" align="center">
					Pré registro de nascimento gerado.
				</Typography>
				<Typography variant="h5" component={'h2'} align="center">
					Seu código é:
				</Typography>
			</Grid>
			<Grid item xs md>
				<Paper elevation={0} style={{ background: 'gray', padding: 10 }}>
					<Typography variant="h5" component="h2" align="center">
						{chave}
					</Typography>
				</Paper>
			</Grid>
		</Container>
	) : (
		<Fragment />
	);
};

export default ResultForm;
