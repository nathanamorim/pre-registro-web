import React, { Fragment, PropsWithChildren } from 'react';
import {
	withStyles,
	Theme,
	createStyles,
	makeStyles,
} from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import { Grid } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			width: '100%',
		},
		heading: {
			fontSize: theme.typography.pxToRem(15),
			fontWeight: theme.typography.fontWeightRegular,
		},
	})
);

type IAccordions = {
	label: string;
	open: boolean;
};

const OneAccordion = (props: PropsWithChildren<IAccordions>) => {
	const classes = useStyles();
	const { label, children, open } = props;

	return (
		<Accordion>
			<AccordionSummary
				expandIcon={<ExpandMoreIcon />}
				aria-controls={`${label}-content`}
				id={`${label}-head`}
			>
				<Typography className={classes.heading}>{label}</Typography>
			</AccordionSummary>
			<AccordionDetails>
				<Grid container spacing={4} alignItems="stretch">
					{children}
				</Grid>
			</AccordionDetails>
		</Accordion>
	);
};
export default OneAccordion;
