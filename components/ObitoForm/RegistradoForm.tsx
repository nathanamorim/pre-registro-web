import React, { Fragment } from 'react';
import {
	Container,
	FormControl,
	FormControlLabel,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	Switch,
	TextField,
	Typography,
} from '@material-ui/core';
import { Controller } from 'react-hook-form';
import { IFormWithReset } from '../../interfaces/IForm';
import MStepperHeader from '../Steppers/MStepperHeader';
import { Data } from '../../shared/utils';
import DadosAdicionaisForm from './DadosAdicionaisForm';
import OutrosDocumentosForm from './OutorsDocumentosForm';

const RegistradoForm = ({
	active,
	control,
	errors,
	current,
	watch,
	reset,
}: IFormWithReset) => {
	const watchEstadoCivil = watch('registrado.estadoCivil');
	const watchDataNascimento = watch('registrado.dataNascimento');
	return !active('registrado') ? (
		<Container style={{ marginBottom: '2rem' }}>
			<MStepperHeader label={'Dados do Registrado'} step={current} />
			<Grid container spacing={4}>
				<Grid item xs={12} md={4}>
					<Controller
						name="registrado.do"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.do}
								helperText={errors?.registrado?.do?.message}
								label="DO"
								id="registrado-do"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={8}>
					<Controller
						name="registrado.nome"
						control={control}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.nome}
								helperText={errors?.registrado?.nome?.message}
								label="Nome completo do falecido"
								id="registrado-nome"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="registrado.dataNascimento"
						control={control}
						render={({ field: { onChange, value, ref } }) => (
							<TextField
								id="registrado-data-nascimento"
								label="Data de nascimento"
								type="date"
								onChange={onChange}
								value={value}
								defaultValue={new Data().now('1930-01-01')}
								inputRef={ref}
								error={!!errors.registrado?.dataNascimento}
								helperText={errors?.registrado?.dataNascimento?.message}
								variant="outlined"
								fullWidth
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={2}>
					<Controller
						name="registrado.idade"
						control={control}
						rules={{ required: false }}
						render={({ field: { onChange } }) => (
							<TextField
								error={!!errors.registrado?.idade}
								helperText={errors?.registrado?.idade?.message}
								label="Idade"
								id="registrado-idade"
								variant="outlined"
								fullWidth
								InputLabelProps={{
									shrink: true,
								}}
								inputProps={{ readOnly: true }}
								value={
									watchDataNascimento && new Data().getAge(watchDataNascimento)
								}
								onChange={onChange}
							/>
						)}
					/>
				</Grid>

				<Grid item xs={12} md={2}>
					<Controller
						name="registrado.sexo"
						control={control}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.registrado?.sexo}
							>
								<InputLabel id="registrado-sexo">Sexo</InputLabel>
								<Select
									id="registrado-sexo"
									{...field}
									defaultValue={''}
									label="Sexo"
								>
									<MenuItem value={'M'}>Masculino</MenuItem>
									<MenuItem value={'F'}>Feminino</MenuItem>
									<MenuItem value={'O'}>Outro</MenuItem>
								</Select>
								<FormHelperText>
									{!!errors.registrado?.sexo &&
										errors?.registrado?.sexo?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={2}>
					<Controller
						name="registrado.cor"
						control={control}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.registrado?.cor}
							>
								<InputLabel>Cor</InputLabel>
								<Select
									id="registrado-cor"
									defaultValue={''}
									{...field}
									label="Cor"
								>
									<MenuItem value={'branca'}>Branca</MenuItem>
									<MenuItem value={'preta'}>Preta</MenuItem>
									<MenuItem value={'parda'}>Parda</MenuItem>
									<MenuItem value={'amarela'}>Amarela</MenuItem>
									<MenuItem value={'ingigena'}>Indígena</MenuItem>
								</Select>
								<FormHelperText>
									{!!errors.registrado?.cor && errors?.registrado?.cor?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={2}>
					<Controller
						name="registrado.estadoCivil"
						control={control}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.registrado?.estadoCivil}
							>
								<InputLabel>Estado civil</InputLabel>
								<Select
									id="registrado-estadoCivil"
									defaultValue={''}
									{...field}
									label="Estado Civil"
								>
									<MenuItem value={'casado'}>Casado(a)</MenuItem>
									<MenuItem value={'divorciado'}>Divorciado(a)</MenuItem>
									<MenuItem value={'viuvo'}>Viuvo(a)</MenuItem>
									<MenuItem value={'separado'}>Separado(a)</MenuItem>
								</Select>
								<FormHelperText>
									{!!errors.registrado?.estadoCivil &&
										errors?.registrado?.estadoCivil?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				{watchEstadoCivil && (
					<Grid item xs={12} md={12}>
						<Controller
							name={
								watchEstadoCivil === 'casado'
									? 'registrado.conjude'
									: 'registrado.exconjude'
							}
							control={control}
							defaultValue={''}
							rules={{ required: false }}
							render={({ field }) => (
								<TextField
									error={
										watchEstadoCivil === 'casado'
											? !!errors.registrado?.conjude
											: !!errors.registrado?.exconjude
									}
									helperText={
										watchEstadoCivil === 'casado'
											? errors?.registrado?.conjude?.message
											: errors?.registrado?.exconjude?.message
									}
									label={
										watchEstadoCivil === 'casado' ? 'Cônjude' : 'Ex cônjude'
									}
									id="registrado-exconjude"
									variant="outlined"
									fullWidth
									{...field}
								/>
							)}
						/>
					</Grid>
				)}

				<Grid item xs={12} md={12}>
					<Controller
						name="registrado.localNascimento"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.localNascimento}
								helperText={errors?.registrado?.localNascimento?.message}
								label="Local de Nascimento"
								id="registrado-localNascimento"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={2}>
					<Controller
						name={`registrado.naturalidade.uf`}
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.filiacao?.naturalidade?.uf}
								helperText={errors?.filiacao?.naturalidade?.uf?.message}
								label="UF Naturalidade"
								id="registrado-naturalidade-uf"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name={`registrado.naturalidade.municipio`}
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.naturalidade?.municipio}
								helperText={
									errors?.registrado?.naturalidade?.municipio?.message
								}
								label="Naturalidade"
								id="registrado-naturalidade-municipio"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={6}>
					<Controller
						name={`registrado.nacionalidade`}
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.nacionalidade}
								helperText={errors?.registrado?.nacionalidade?.message}
								label="Nacionalidade"
								id={`registrado-nacionalidade`}
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={12}>
					<Controller
						name="registrado.profissao"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.profissao}
								helperText={errors?.registrado?.profissao?.message}
								label="Profissão"
								id="registrado-profissao"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={12}>
					<Controller
						name="registrado.endereco"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.endereco}
								helperText={errors?.registrado?.endereco?.message}
								label="Endereço domicílio"
								id="registrado-endereco"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="registrado.gemeos"
						control={control}
						render={({ field: { onChange, value } }) => (
							<FormControlLabel
								control={
									<Switch color="primary" checked={value} onChange={onChange} />
								}
								label="Gêmeos"
							/>
						)}
					/>
				</Grid>
			</Grid>
			<DadosAdicionaisForm
				errors={errors}
				control={control}
				watch={watch}
				reset={reset}
			/>
			<OutrosDocumentosForm errors={errors} control={control} watch={watch} />
		</Container>
	) : (
		<Fragment />
	);
};

export default RegistradoForm;
