import React from 'react';
import { Grid, TextField, Typography } from '@material-ui/core';
import { Controller } from 'react-hook-form';
import { IFormItem } from '../../interfaces/IForm';

const OutrosDocumentosForm = ({ control, errors, watch }: IFormItem) => {
	const [qtdFilhos, setQtdFilhos] = React.useState([{ id: 0 }]);
	const [count, setCount] = React.useState(0);

	return (
		<Grid container spacing={4}>
			<Grid item xs={12} md={12}>
				<Typography
					variant={'subtitle1'}
					style={{ borderBottom: '1px solid #7dcd52', margin: '3px 0px' }}
				>
					Outros Documentos
				</Typography>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name={`registrado.outros.pis`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.registrado?.outros?.pis}
							helperText={errors?.registrado?.outros?.pis?.message}
							label="Número de inscrição no PIS/PASEP"
							id={`registrado-outros-pis`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name={`registrado.outros.inss`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.registrado?.outros?.inss}
							helperText={errors?.registrado?.outros?.inss?.message}
							label="Número de inscrição no INSS, se contribuinte individual"
							id={`registrado-outros-inss`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name={`registrado.outros.previdencia`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.registrado?.outros?.previdencia}
							helperText={errors?.registrado?.outros?.previdencia?.message}
							label="Número de benefício previdenciário"
							id={`registrado-outros-previdencia`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name={`registrado.outros.tituloEleitor`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.registrado?.outros?.tituloEleitor}
							helperText={errors?.registrado?.outros?.tituloEleitor?.message}
							label="Número do título de eleitor"
							id={`registrado-outros-tituloEleitor`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name={`registrado.outros.registroNascCasamento`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.registrado?.outros?.registroNascCasamento}
							helperText={
								errors?.registrado?.outros?.registroNascCasamento?.message
							}
							label="Registro de nascimento e casamento: Livro, Folha e Termo e o Cartório de Registro Civil das Pessoas Naturais"
							id={`registrado-outros-registroNascCasamento`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name={`registrado.outros.carteiraTrabalho`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.registrado?.outros?.carteiraTrabalho}
							helperText={errors?.registrado?.outros?.carteiraTrabalho?.message}
							label="Número e série da Carteira de Trabalho e Previdência Social - CTPS"
							id={`registrado-outros-carteiraTrabalho`}
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
		</Grid>
	);
};

export default OutrosDocumentosForm;
