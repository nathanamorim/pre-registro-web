import React, { Fragment } from 'react';
import {
	Button,
	Checkbox,
	Container,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormLabel,
	Grid,
	IconButton,
	InputLabel,
	MenuItem,
	Radio,
	RadioGroup,
	Select,
	Switch,
	TextField,
	Typography,
} from '@material-ui/core';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import { Controller } from 'react-hook-form';
import { IFormItemWithReset } from '../../interfaces/IForm';
import MStepperHeader from '../Steppers/MStepperHeader';
import { ISelect } from '../../interfaces/ISelect';
import { Data } from '../../shared/utils';

enum EnumLocalCasamento {
	cartorio = 'cartorio',
	domicilio = 'domicilio',
	religioso = 'religioso',
}

const tipoLocalFalecimento: ISelect[] = [
	{
		label: 'Hospital',
		value: 'hospital',
	},
	{
		label: 'Residência',
		value: 'residencia',
	},
	{
		label: 'Outro',
		value: 'outro',
	},
];

const RegimeDeBens: ISelect[] = [
	{ label: 'Separação de bens', value: 'separacao' },
	{ label: 'Comunhão parcial', value: 'comunhao-parcial' },
	{
		label: 'Comunhão universal',
		value: 'comunhao-universal',
	},
	{ label: 'Participação final dos aquestos', value: 'final-aquestos' },
];

const DadosAdicionaisForm = ({
	control,
	errors,
	watch,
	reset,
}: IFormItemWithReset) => {
	const watchFilho = watch('registrado.adicionais.filho');
	const watchFilhos = watch('registrado.adicionais.filhos');
	const [qtdFilhos, setQtdFilhos] = React.useState([{ id: 0 }]);
	const [count, setCount] = React.useState(0);
	const handleQtdFilhos = () => {
		setQtdFilhos(() => [...qtdFilhos, { id: count + 1 }]);
		setCount(count + 1);
	};

	const handleRemoveQtdFilhos = (id: number): void => {
		let filtered = qtdFilhos.filter((x) => x.id !== id);
		setQtdFilhos(filtered);
		if (count > 0) setCount(count - 1);
	};

	return (
		<Grid container spacing={4}>
			<Grid item xs={12} md={12}>
				<Typography
					variant={'subtitle1'}
					style={{ borderBottom: '1px solid #7dcd52', margin: '3px 0px' }}
				>
					Dados Adicionais
				</Typography>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name="registrado.adicionais.eleitor"
					control={control}
					render={({ field: { onChange, value } }) => (
						<FormControlLabel
							control={
								<Checkbox color="primary" checked={value} onChange={onChange} />
							}
							label="Era eleitor"
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name="registrado.adicionais.bens"
					control={control}
					render={({ field: { onChange, value } }) => (
						<FormControlLabel
							control={
								<Checkbox color="primary" checked={value} onChange={onChange} />
							}
							label="Deixou bens"
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={3}>
				<Controller
					name="registrado.adicionais.testamento"
					control={control}
					render={({ field: { onChange, value } }) => (
						<FormControlLabel
							control={
								<Checkbox color="primary" checked={value} onChange={onChange} />
							}
							label="Deixou testamento"
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name="registrado.adicionais.herdeiros"
					control={control}
					render={({ field: { onChange, value } }) => (
						<FormControlLabel
							control={
								<Checkbox color="primary" checked={value} onChange={onChange} />
							}
							label="Deixou herdeiros menores ou interditos"
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name="registrado.adicionais.filho"
					control={control}
					render={({ field: { onChange, value } }) => (
						<FormControlLabel
							control={
								<Checkbox color="primary" checked={value} onChange={onChange} />
							}
							label="Deixou filhos"
						/>
					)}
				/>
			</Grid>

			{watchFilho &&
				qtdFilhos?.map((f) => (
					<Fragment key={f.id}>
						<Grid item xs={12} md={8}>
							<Controller
								name={`registrado.adicionais.filhos[${f.id}].nome`}
								control={control}
								defaultValue={''}
								rules={{ required: false }}
								render={({ field }) => (
									<TextField
										error={
											!!errors.registrado?.adicionais?.filhos?.[f.id]?.nome
										}
										helperText={
											errors?.registrado?.adicionais?.filhos?.[f.id]?.nome
												?.message
										}
										label="Nome do(a) filhos(a)"
										id={`registrado-adicionais-filhos-[${f.id}]-nome`}
										variant="outlined"
										fullWidth
										{...field}
									/>
								)}
							/>
						</Grid>
						<Grid item xs={12} md={3}>
							<Controller
								name={`registrado.adicionais.filhos[${f.id}].idade`}
								control={control}
								defaultValue={''}
								rules={{ required: false }}
								render={({ field }) => (
									<TextField
										error={
											!!errors.registrado?.adicionais?.filhos?.[f.id]?.idade
										}
										helperText={
											errors?.registrado?.adicionais?.filhos?.[f.id]?.idade
												?.message
										}
										label="Idade do(a) filhos(a)"
										id={`registrado-adicionais-filhos-[${f.id}]-idade`}
										variant="outlined"
										fullWidth
										{...field}
									/>
								)}
							/>
						</Grid>
						<Grid item xs={12} md={1}>
							<IconButton
								color="primary"
								onClick={() => handleRemoveQtdFilhos(f.id)}
							>
								<RemoveCircleIcon />
							</IconButton>
						</Grid>
					</Fragment>
				))}
			{watchFilho && (
				<Grid item xs={12} md={12}>
					<Button onClick={handleQtdFilhos} color="primary" variant="contained">
						Adicionar Filho
					</Button>
				</Grid>
			)}
		</Grid>
	);
};

export default DadosAdicionaisForm;
