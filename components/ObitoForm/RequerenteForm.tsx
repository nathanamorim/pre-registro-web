import React, { Fragment } from 'react';
import {
	Container,
	FormControl,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	TextField,
} from '@material-ui/core';
import { Controller } from 'react-hook-form';
import { IForm } from '../../interfaces/IForm';
import MStepperHeader from '../Steppers/MStepperHeader';
import InputCPFMask from '../InputCPFMask';
import InputTelefoneMask from '../InputTelefoneMask';
type EstadosProps = {
	estados: [];
};
type FormRequerenteType = IForm & EstadosProps;
const RequerenteForm = ({ errors, active, control, current, watch }: IForm) => {
	return !active('requerente') ? (
		<Container>
			<MStepperHeader label={'Requerente'} step={current} />
			<Grid container spacing={4} alignItems="stretch">
				<Grid item xs={12} md={6}>
					<Controller
						name="requerente.cpf"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.cpf}
								helperText={errors?.requerente?.cpf?.message}
								label="CPF"
								id="requerente-cpf"
								variant="outlined"
								fullWidth
								InputProps={{
									inputComponent: InputCPFMask as any,
								}}
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={6}>
					<Controller
						name="requerente.rg"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.rg}
								helperText={errors?.requerente?.rg?.message}
								label="RG"
								id="filiacao-rg"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={12}>
					<Controller
						name="requerente.nome"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.nome}
								helperText={errors?.requerente?.nome?.message}
								label="Nome Completo"
								id="filiacao-nome"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={2}>
					<Controller
						name="requerente.estadoCivil"
						control={control}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.requerente?.estadoCivil}
							>
								<InputLabel>Estado civil</InputLabel>
								<Select
									id="requerente-estadoCivil"
									defaultValue={''}
									{...field}
									label="Estado Civil"
								>
									<MenuItem value={'casado'}>Casado(a)</MenuItem>
									<MenuItem value={'divorciado'}>Divorciado(a)</MenuItem>
									<MenuItem value={'separado'}>Separado(a)</MenuItem>
								</Select>
								<FormHelperText>
									{!!errors.requerente?.estadoCivil &&
										errors?.requerente?.estadoCivil?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="requerente.telefone"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.telefone}
								helperText={errors?.requerente?.telefone?.message}
								label="Telefone"
								id="filiacao-telefone"
								variant="outlined"
								fullWidth
								InputProps={{
									inputComponent: InputTelefoneMask as any,
								}}
								{...field}
							/>
						)}
					/>
				</Grid>

				<Grid item xs={12} md={6}>
					<Controller
						name="requerente.email"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.email}
								helperText={errors?.requerente?.email?.message}
								label="E-mail"
								id="filiacao-email"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={2}>
					<Controller
						name="requerente.naturalidadeUf"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.naturalidadeUf}
								helperText={errors?.requerente?.naturalidadeUf?.message}
								label="UF Naturalidade"
								id="requerente-naturalidadeUf"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="requerente.naturalidade"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.naturalidade}
								helperText={errors?.requerente?.naturalidade?.message}
								label="Naturalidade"
								id="filiacao-naturalidade"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={6}>
					<Controller
						name="requerente.nacionalidade"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.nacionalidade}
								helperText={errors?.requerente?.nacionalidade?.message}
								label="Nacionalidade"
								id="filiacao-nacionalidade"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>

				<Grid item xs={12} md={3}>
					<Controller
						name="requerente.dataNascimento"
						control={control}
						render={({ field }) => (
							<TextField
								id="filiacao-data-nascimento"
								label="Data de nascimento"
								type="date"
								{...field}
								error={!!errors.requerente?.dataNascimento}
								helperText={errors?.requerente?.dataNascimento?.message}
								variant="outlined"
								defaultValue="1901-01-01"
								fullWidth
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="requerente.profissao"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.profissao}
								helperText={errors?.requerente?.profissao?.message}
								label="Profissão"
								id="filiacao-profissao"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={5}>
					<Controller
						name="requerente.parentesco"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.parentesco}
								helperText={errors?.requerente?.parentesco?.message}
								label="Grau de parentesco"
								id="requerente-parentesco"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={12}>
					<Controller
						name="requerente.endereco"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.requerente?.endereco}
								helperText={errors?.requerente?.endereco?.message}
								label="Endereco"
								id="filiacao-endereco"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
			</Grid>
		</Container>
	) : (
		<Fragment />
	);
};
export default RequerenteForm;
