import React, { Fragment } from 'react';
import {
	Container,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormLabel,
	Grid,
	InputLabel,
	MenuItem,
	Radio,
	RadioGroup,
	Select,
	TextField,
	Typography,
} from '@material-ui/core';
import { Controller } from 'react-hook-form';
import { IForm } from '../../interfaces/IForm';
import MStepperHeader from '../Steppers/MStepperHeader';
import { ISelect } from '../../interfaces/ISelect';
import { Data } from '../../shared/utils';

enum EnumLocalCasamento {
	cartorio = 'cartorio',
	domicilio = 'domicilio',
	religioso = 'religioso',
}

const tipoLocalFalecimento: ISelect[] = [
	{
		label: 'Hospital',
		value: 'hospital',
	},
	{
		label: 'Residência',
		value: 'residencia',
	},
	{
		label: 'Outro',
		value: 'outro',
	},
];

const RegimeDeBens: ISelect[] = [
	{ label: 'Separação de bens', value: 'separacao' },
	{ label: 'Comunhão parcial', value: 'comunhao-parcial' },
	{
		label: 'Comunhão universal',
		value: 'comunhao-universal',
	},
	{ label: 'Participação final dos aquestos', value: 'final-aquestos' },
];

const FalecimentoForm = ({
	active,
	control,
	errors,
	current,
	watch,
}: IForm) => {
	const watchTipoLocal = watch('falecimento.tipoLocal');
	const watchCremacao = watch('falecimento.cremacao');
	return !active('falecimento') ? (
		<Container style={{ marginBottom: '2rem' }}>
			<MStepperHeader label={'Dados do Falecimento'} step={current} />
			<Grid container spacing={4}>
				<Grid item xs={12} md={3}>
					<Controller
						name="falecimento.tipoLocal"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.falecimento?.tipoLocal}
							>
								<InputLabel>Tipo do Local de Falecimento</InputLabel>
								<Select id="falecimento-tipo-local" {...field}>
									{tipoLocalFalecimento.map((tipo) => (
										<MenuItem key={tipo.value} value={tipo.value}>
											{tipo.label}
										</MenuItem>
									))}
								</Select>
								<FormHelperText>
									{!!errors.falecimento?.tipoLocal &&
										errors?.falecimento?.tipoLocal?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				{watchTipoLocal == 'outro' && (
					<Grid item xs={12} md={9}>
						<Controller
							name="falecimento.outroLocal"
							control={control}
							render={({ field }) => (
								<TextField
									error={!!errors.falecimento?.outroLocal}
									helperText={errors?.falecimento?.outroLocal?.message}
									label="Outro local de falecimento"
									id="falecimento-outroLocal"
									variant="outlined"
									fullWidth
									{...field}
								/>
							)}
						/>
					</Grid>
				)}
				<Grid item xs={12} md={watchTipoLocal === 'outro' ? 12 : 9}>
					<Controller
						name="falecimento.local"
						control={control}
						render={({ field }) => (
							<TextField
								error={!!errors.falecimento?.local}
								helperText={errors?.falecimento?.local?.message}
								label="Local de falecimento"
								id="falecimento-local"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="falecimento.tipoMorte"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.falecimento?.tipoMorte}
							>
								<InputLabel>Tipo de morte</InputLabel>
								<Select id="falecimento-tipoMorte" {...field}>
									<MenuItem value={'natural'}>Natural</MenuItem>
									<MenuItem value={'violenta'}>Violenta</MenuItem>
								</Select>
								<FormHelperText>
									{!!errors.falecimento?.tipoMorte &&
										errors?.falecimento?.tipoMorte?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="falecimento.data"
						control={control}
						render={({ field: { onChange, value, ref } }) => (
							<TextField
								id="falecimento-data"
								label="Data e Hora do Falecimento"
								type="datetime-local"
								defaultValue={new Data().now('yyyy-MM-DDTHH:mm')}
								onChange={onChange}
								value={value}
								inputRef={ref}
								error={!!errors.falecimento?.data}
								helperText={errors?.falecimento?.data?.message}
								variant="outlined"
								fullWidth
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="falecimento.cremacao"
						control={control}
						render={({ field: { onChange, value, ref } }) => (
							<FormControl component="fieldset">
								<FormLabel component="legend">Cremação</FormLabel>
								<RadioGroup
									aria-label="cremacao"
									value={value}
									onChange={onChange}
									defaultValue={'nao'}
									row
								>
									<FormControlLabel
										value={'sim'}
										control={<Radio color="primary" />}
										label="Sim"
									/>
									<FormControlLabel
										value={'nao'}
										control={<Radio color="primary" />}
										label="Não"
									/>
								</RadioGroup>
							</FormControl>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={12}>
					<Controller
						name={
							watchCremacao === 'sim'
								? 'falecimento.localCremacao'
								: 'falecimento.localSeputamento'
						}
						control={control}
						render={({ field }) => (
							<TextField
								error={!!errors.falecimento?.local}
								helperText={errors?.falecimento?.local?.message}
								label={
									watchCremacao === 'sim'
										? 'Local de cremação'
										: 'Local de seputamento'
								}
								id="falecimento-local-seputamento-cremacao"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={12}>
					<Typography>Informações médicas</Typography>
				</Grid>
				<Grid item xs={12} md={12}>
					<Controller
						name={`falecimento.causa[${1}]`}
						control={control}
						render={({ field }) => (
							<TextField
								error={!!errors.falecimento?.causa[1]}
								helperText={errors?.falecimento?.causa[1]?.message}
								label={'Causa do falecimento'}
								id="falecimento-causa-falecimento"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>

				<Grid item xs={12} md={6}>
					<Controller
						name={`falecimento.medico[${1}].nome`}
						control={control}
						render={({ field }) => (
							<TextField
								error={!!errors.falecimento?.medico?.[1]?.nome}
								helperText={errors?.falecimento?.medico?.[1]?.nome?.message}
								label={'Médico que atestou o óbito'}
								id="falecimento-medico-atestou"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={6}>
					<Controller
						name={`falecimento.medico[${1}].crm`}
						control={control}
						render={({ field }) => (
							<TextField
								error={!!errors.falecimento?.medico?.[1]?.crm}
								helperText={errors?.falecimento?.medico?.[1]?.crm?.message}
								label={'CRM do médico que atestou o óbito'}
								id="falecimento-medico-atestou"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				{watchCremacao === 'sim' && (
					<Grid item xs={12} md={12}>
						<Controller
							name={`falecimento.legista`}
							control={control}
							render={({ field }) => (
								<TextField
									error={!!errors.falecimento?.legista}
									helperText={errors?.falecimento?.legista?.message}
									label={'Nome do médico legista'}
									id="falecimento-legista"
									variant="outlined"
									fullWidth
									{...field}
								/>
							)}
						/>
					</Grid>
				)}
			</Grid>
		</Container>
	) : (
		<Fragment />
	);
};

export default FalecimentoForm;
