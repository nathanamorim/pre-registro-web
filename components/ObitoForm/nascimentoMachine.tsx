import { createMachine, assign } from 'xstate';
// import { submit } from './utils';
const submit = (ctx: MContext) => {
	console.log('Submitting', ctx);

	return new Promise((resolve, reject) => {
		setTimeout(() => {
			return Math.random() < 0.4 ? reject() : resolve(ctx);
		}, 1000);
	});
};

interface MContext {
	requerente: {};
	registrado: {};
	filiacao: {};
	avos: {};
}

type MEvent = { type: 'NEXT'; data: {} } | { type: 'RETRY' };

export const nascimentoMachine = createMachine<MContext, MEvent>({
	id: 'nascimento',
	context: {
		requerente: {},
		registrado: {},
		filiacao: {},
		avos: {},
	},
	initial: 'requerente',
	states: {
		requerente: {
			on: {
				NEXT: {
					target: 'registrado',
					actions: assign({ requerente: (ctx, e) => e.data }),
				},
			},
		},
		registrado: {
			on: {
				NEXT: {
					target: 'filiacao',
					actions: assign({ registrado: (ctx, e) => e.data }),
				},
			},
		},
		filiacao: {
			on: {
				NEXT: {
					target: 'avos',
					actions: assign({ filiacao: (ctx, e) => e.data }),
				},
			},
		},
		avos: {
			on: {
				NEXT: {
					target: 'avos',
					actions: assign({ avos: (ctx, e) => e.data }),
				},
			},
		},
		loading: {
			invoke: {
				id: 'submitting',
				src: (ctx) => submit(ctx),
				onDone: 'success',
				onError: 'failure',
			},
		},
		failure: {
			on: {
				RETRY: 'loading',
			},
		},
		success: {
			type: 'final',
		},
	},
});
