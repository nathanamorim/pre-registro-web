import { Grid, TextField } from '@material-ui/core';
import axios, { AxiosResponse } from 'axios';
import React, { Fragment } from 'react';
import {
	Control,
	Controller,
	DeepMap,
	FieldError,
	UseFormSetValue,
	UseFormWatch,
} from 'react-hook-form';

interface IFormItem {
	errors: DeepMap<any, FieldError>;
	control: Control<any>;
	watch: UseFormWatch<any>;
}

interface IFormItemWithSetValue extends IFormItem {
	setValue: UseFormSetValue<any>;
	id?: number | null;
}

type EnderecoType = {
	cep?: string;
	logradouro?: string;
	complemento?: string;
	bairro?: string;
	localidade?: string;
	uf?: string;
	numero?: string;
};

const obterEndereco = async ({ cep }: EnderecoType) => {
	if (cep && cep.length === 8) {
		return await axios
			.get(`https://viacep.com.br/ws/${cep}/json/unicode/`)
			.then(({ data }: AxiosResponse<any>) => data as EnderecoType)
			.catch((err) => console.error(err.message));
	}
};

type EnderecoFormType = {
	node: string;
} & IFormItemWithSetValue;

const EnderecoFields = ({
	control,
	setValue,
	watch,
	errors,
	node,
}: EnderecoFormType) => {
	const watchCep = watch(`${node}.endereco.cep`);
	React.useEffect(() => carregarEndereco(watchCep), [watchCep]);

	const carregarEndereco = (cep: string): void => {
		obterEndereco({ cep: cep }).then((data: EnderecoType | void) => {
			setValue(`${node}.endereco.logradouro`, data?.logradouro);
			setValue(`${node}.endereco.bairro`, data?.bairro);
			setValue(`${node}.endereco.localidade`, data?.localidade);
			setValue(`${node}.endereco.uf`, data?.uf);
		});
	};
	return (
		<Fragment>
			<Grid item xs={12} md={3}>
				<Controller
					name={`${node}.endereco.cep`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors?.[node]?.endereco?.cep}
							helperText={errors?.[node]?.endereco?.cep?.message}
							label="CEP"
							id="endereco-cep"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={7}>
				<Controller
					name={`${node}.endereco.logradouro`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors?.[node]?.endereco?.logradouro}
							helperText={errors?.[node]?.endereco?.logradouro?.message}
							label="Logradouro"
							id="endereco-logradouro"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name={`${node}.endereco.numero`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors?.[node]?.endereco?.numero}
							helperText={errors?.[node]?.endereco?.numero?.message}
							label="Número"
							id="endereco-numero"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name={`${node}.endereco.complemento`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors?.[node]?.endereco?.complemento}
							helperText={errors?.[node]?.endereco?.complemento?.message}
							label="Complemento"
							id="endereco-complemento"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name={`${node}.endereco.uf`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors?.[node]?.endereco?.uf}
							helperText={errors?.[node]?.endereco?.uf?.message}
							label="Estado"
							id="endereco-estado"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>

			<Grid item xs={12} md={3}>
				<Controller
					name={`${node}.endereco.localidade`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors?.[node]?.endereco?.localidade}
							helperText={errors?.[node]?.endereco?.localidade?.message}
							label="Cidade"
							id="endereco-cidade"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={3}>
				<Controller
					name={`${node}.endereco.bairro`}
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors?.[node]?.endereco?.bairro}
							helperText={errors?.[node]?.endereco?.bairro?.message}
							label="Bairro"
							id="endereco-bairro"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
		</Fragment>
	);
};

export default EnderecoFields;
