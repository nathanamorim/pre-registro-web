import { Container, Grid, TextField, Typography } from '@material-ui/core';
import React, { Fragment } from 'react';
import { Controller } from 'react-hook-form';
import { IForm, IFormItem } from '../../interfaces/IForm';
import CustomizedAccordions from '../OneAccordion';
import InputCPFMask from '../InputCPFMask';
import InputTelefoneMask from '../InputTelefoneMask';
import MStepperHeader from '../Steppers/MStepperHeader';
import OneAccordion from '../OneAccordion';

const FiliacaoForm = ({ active, control, errors, current, watch }: IForm) => {
	return !active('filiacao') ? (
		<Container style={{ marginBottom: '2rem' }}>
			<MStepperHeader label={'Requerente'} step={current} />
			<OneAccordion label={'Dados da Mãe'} open={true}>
				<FiliacaoFormMae errors={errors} control={control} watch={watch} />
			</OneAccordion>
			<OneAccordion label={'Dados do Pai'} open={false}>
				<FiliacaoFormPai errors={errors} control={control} watch={watch} />
			</OneAccordion>
		</Container>
	) : (
		<Fragment />
	);
};

export default FiliacaoForm;

export const FiliacaoFormMae = ({ control, errors, watch }: IFormItem) => {
	const watchFalecido = watch('filiacao.mae.falecido');

	return (
		<Fragment>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.mae.cpf"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.cpf}
							helperText={errors?.filiacao?.mae?.cpf?.message}
							label="CPF"
							id="filiacao-mae-cpf"
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputCPFMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.mae.rg"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.rg}
							helperText={errors?.filiacao?.mae?.rg?.message}
							label="RG"
							id="filiacao-rg"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name="filiacao.mae.nome"
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.nome}
							helperText={errors?.filiacao?.mae?.nome?.message}
							label="Nome Completo"
							id="filiacao-nome"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.mae.telefone"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.telefone}
							helperText={errors?.filiacao?.mae?.telefone?.message}
							label="Telefone"
							id="filiacao-telefone"
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputTelefoneMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>

			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.mae.email"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.email}
							helperText={errors?.filiacao?.mae?.email?.message}
							label="E-mail"
							id="filiacao-email"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name="filiacao.mae.naturalidadeUf"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.naturalidadeUf}
							helperText={errors?.filiacao?.mae?.naturalidadeUf?.message}
							label="UF Naturalidade"
							id="filiacao-mae-naturalidadeUf"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name="filiacao.mae.naturalidade"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.naturalidade}
							helperText={errors?.filiacao?.mae?.naturalidade?.message}
							label="Naturalidade"
							id="filiacao-naturalidade"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.mae.nacionalidade"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.nacionalidade}
							helperText={errors?.filiacao?.mae?.nacionalidade?.message}
							label="Nacionalidade"
							id="filiacao-nacionalidade"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>

			<Grid item xs={12} md={3}>
				<Controller
					name="filiacao.mae.dataNascimento"
					control={control}
					render={({ field }) => (
						<TextField
							id="filiacao-data-nascimento"
							label="Data de nascimento"
							type="date"
							{...field}
							error={!!errors.filiacao?.mae?.dataNascimento}
							helperText={errors?.filiacao?.mae?.dataNascimento?.message}
							variant="outlined"
							fullWidth
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={9}>
				<Controller
					name="filiacao.mae.profissao"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.profissao}
							helperText={errors?.filiacao?.mae?.profissao?.message}
							label="Profissão"
							id="filiacao-profissao"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name="filiacao.mae.endereco"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.mae?.endereco}
							helperText={errors?.filiacao?.mae?.endereco?.message}
							label="Endereco"
							id="filiacao-endereco"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
		</Fragment>
	);
};

const FiliacaoFormPai = ({ control, errors, watch }: IFormItem) => {
	const watchFalecido = watch('filiacao.pai.falecido');

	return (
		<Fragment>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.cpf"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.cpf}
							helperText={errors?.filiacao?.pai?.cpf?.message}
							label="CPF"
							id="filiacao-cpf"
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputCPFMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.rg"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.rg}
							helperText={errors?.filiacao?.pai?.rg?.message}
							label="RG"
							id="filiacao-rg"
							variant="outlined"
							placeholder="___.___.___-__"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name="filiacao.pai.nome"
					control={control}
					defaultValue={''}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.nome}
							helperText={errors?.filiacao?.pai?.nome?.message}
							label="Nome Completo"
							id="filiacao-pai-nome"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.telefone"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.telefone}
							helperText={errors?.filiacao?.pai?.telefone?.message}
							label="Telefone"
							id="filiacao-pai-telefone"
							variant="outlined"
							fullWidth
							InputProps={{
								inputComponent: InputTelefoneMask as any,
							}}
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.email"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.email}
							helperText={errors?.filiacao?.pai?.email?.message}
							label="E-mail"
							id="filiacao-pai-email"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={2}>
				<Controller
					name="filiacao.pai.naturalidadeUf"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.naturalidadeUf}
							helperText={errors?.filiacao?.pai?.naturalidadeUf?.message}
							label="UF Naturalidade"
							id="filiacao-pai-naturalidadeUf"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name="filiacao.pai.naturalidade"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.naturalidade}
							helperText={errors?.filiacao?.pai?.naturalidade?.message}
							label="Naturalidade"
							id="filiacao-naturalidade"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={6}>
				<Controller
					name="filiacao.pai.nacionalidade"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.nacionalidade}
							helperText={errors?.filiacao?.pai?.nacionalidade?.message}
							label="Nacionalidade"
							id="filiacao-nacionalidade"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={4}>
				<Controller
					name="filiacao.pai.dataNascimento"
					control={control}
					render={({ field }) => (
						<TextField
							id="filiacao-data-nascimento"
							label="Data de nascimento"
							type="date"
							{...field}
							error={!!errors.filiacao?.pai?.dataNascimento}
							helperText={errors?.filiacao?.pai?.dataNascimento?.message}
							variant="outlined"
							fullWidth
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={8}>
				<Controller
					name="filiacao.pai.profissao"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.profissao}
							helperText={errors?.filiacao?.pai?.profissao?.message}
							label="Profissão"
							id="filiacao-pai-profissao"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
			<Grid item xs={12} md={12}>
				<Controller
					name="filiacao.pai.endereco"
					control={control}
					rules={{ required: false }}
					render={({ field }) => (
						<TextField
							error={!!errors.filiacao?.pai?.endereco}
							helperText={errors?.filiacao?.pai?.endereco?.message}
							label="Endereco"
							id="filiacao-endereco"
							variant="outlined"
							fullWidth
							{...field}
						/>
					)}
				/>
			</Grid>
		</Fragment>
	);
};
