import React, { Fragment } from 'react';
import moment from 'moment';
import {
	Container,
	FormControl,
	FormControlLabel,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	Switch,
	TextField,
	Typography,
} from '@material-ui/core';
import { Controller } from 'react-hook-form';
import { IForm } from '../../interfaces/IForm';
import MStepperHeader from '../Steppers/MStepperHeader';
import { ISelect } from '../../interfaces/ISelect';
import {
	EnumNaturalidadeNascimento,
	EnumTipoLocalNascimento,
} from '../../shared/enum';
import InputDeclaracaoMask from '../InputDeclaracaoMask';

const NaturalidadeNascimento: ISelect[] = [
	{
		label: 'Local de Nascimento',
		value: EnumNaturalidadeNascimento.LocalNascimento,
	},
	{
		label: 'Endereço de Residência da Mãe',
		value: EnumNaturalidadeNascimento.EnderecoMae,
	},
];

const TipoLocalNascimento: ISelect[] = [
	{ label: 'Domicílio', value: EnumTipoLocalNascimento.Domicilio },
	{ label: 'Hospital', value: EnumTipoLocalNascimento.Hospital },
	{
		label: 'Estabelecimento de Saúde',
		value: EnumTipoLocalNascimento.EstabelecimentoSaude,
	},
	{ label: 'Via Pública', value: EnumTipoLocalNascimento.ViaPublica },
	{ label: 'Outro', value: EnumTipoLocalNascimento.Outro },
];

const RegistradoForm = ({ active, control, errors, current, watch }: IForm) => {
	const watchTipoLocal = watch('registrado.tipoLocal');
	const [labelLocalNasc, setLabelLocalNasc] = React.useState(
		'Local de Nascimento'
	);

	React.useEffect(
		() => carregaLabelLocalNascimento(watchTipoLocal),
		[watchTipoLocal]
	);

	const carregaLabelLocalNascimento = (tipoLocal: number) => {
		switch (tipoLocal) {
			case EnumTipoLocalNascimento.Hospital:
				setLabelLocalNasc('Informe o nome do hospital');
				break;
			case EnumTipoLocalNascimento.EstabelecimentoSaude:
				setLabelLocalNasc('Informe o nome do estabelecimento de saúde');
				break;
			case EnumTipoLocalNascimento.Outro:
				setLabelLocalNasc('Informe o endereço de nascimento');
				break;
			default:
				setLabelLocalNasc('Informe o endereço de nascimento');
				break;
		}
	};
	return !active('registrado') ? (
		<Container style={{ marginBottom: '2rem' }}>
			<MStepperHeader label={'Requerente'} step={current} />
			<Grid container spacing={4}>
				<Grid item xs={12} md={4}>
					<Controller
						name="registrado.dnv"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.dnv}
								helperText={errors?.registrado?.dnv?.message}
								label="DNV"
								id="registrado-dnv"
								variant="outlined"
								InputProps={{
									inputComponent: InputDeclaracaoMask as any,
								}}
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={8}>
					<Controller
						name="registrado.nome"
						control={control}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.nome}
								helperText={errors?.registrado?.nome?.message}
								label="Nome Completo da Criança"
								id="registrado-nome"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="registrado.sexo"
						control={control}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.registrado?.sexo}
							>
								<InputLabel id="registrado-sexo">Sexo</InputLabel>
								<Select
									id="registrado-sexo"
									{...field}
									defaultValue={''}
									label="Sexo"
								>
									<MenuItem value={'M'}>Masculino</MenuItem>
									<MenuItem value={'F'}>Feminino</MenuItem>
								</Select>
								<FormHelperText>
									{!!errors.registrado?.sexo &&
										errors?.registrado?.sexo?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={8}>
					<Controller
						name="registrado.dataNascimento"
						control={control}
						render={({ field: { onChange, value, ref } }) => (
							<TextField
								id="registrado-data-nascimento"
								label="Data e Hora de nascimento"
								type="datetime-local"
								onChange={onChange}
								value={value}
								inputRef={ref}
								error={!!errors.registrado?.dataNascimento}
								helperText={errors?.registrado?.dataNascimento?.message}
								variant="outlined"
								fullWidth
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="registrado.tipoLocal"
						control={control}
						rules={{ required: false }}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.registrado?.tipoLocal}
							>
								<InputLabel>Tipo do Local de Nascimento</InputLabel>
								<Select id="registrado-tipo-local" {...field} defaultValue={''}>
									{TipoLocalNascimento.map((tipo) => (
										<MenuItem key={tipo.value} value={tipo.value}>
											{tipo.label}
										</MenuItem>
									))}
								</Select>
								<FormHelperText>
									{!!errors.registrado?.tipoLocal &&
										errors?.registrado?.tipoLocal?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>

				<Grid item xs={12} md={8}>
					<Controller
						name="registrado.localNascimento"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.localNascimento}
								helperText={errors?.registrado?.localNascimento?.message}
								label={labelLocalNasc}
								id="registrado-localNascimento"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>

				<Grid item xs={12} md={12}>
					<Controller
						name="registrado.endereco"
						control={control}
						defaultValue={''}
						rules={{ required: false }}
						render={({ field }) => (
							<TextField
								error={!!errors.registrado?.endereco}
								helperText={errors?.registrado?.endereco?.message}
								label="Endereço"
								id="registrado-endereco"
								variant="outlined"
								fullWidth
								{...field}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} md={4}>
					<Controller
						name="registrado.gemeos"
						control={control}
						render={({ field: { onChange, value } }) => (
							<FormControlLabel
								control={
									<Switch color="primary" checked={value} onChange={onChange} />
								}
								label="Gêmeos"
							/>
						)}
					/>
				</Grid>
			</Grid>
		</Container>
	) : (
		<Fragment />
	);
};

export default RegistradoForm;
