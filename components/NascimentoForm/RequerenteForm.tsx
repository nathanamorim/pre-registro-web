import React, { Fragment } from 'react';
import {
	Container,
	FormControl,
	FormHelperText,
	Grid,
	InputLabel,
	MenuItem,
	OutlinedInput,
	Select,
	TextField,
} from '@material-ui/core';
import NumberFormat from 'react-number-format';
import MaterialInput from '@material-ui/core/Input';
import { Controller } from 'react-hook-form';
import { IForm } from '../../interfaces/IForm';
import MStepperHeader from '../Steppers/MStepperHeader';
import InputCPFMask from '../InputCPFMask';
import InputTelefoneMask from '../InputTelefoneMask';

const RequerenteForm = ({ errors, active, control, current, watch }: IForm) => {
	const watchParentesco = watch('requerente.parentesco');
	return !active('requerente') ? (
		<Container>
			<MStepperHeader label={'Requerente'} step={current} />
			<Grid container spacing={4} alignItems="stretch">
				<Grid item xs={12} md={4}>
					<Controller
						name="requerente.parentesco"
						control={control}
						render={({ field }) => (
							<FormControl
								variant="outlined"
								fullWidth
								error={!!errors.requerente?.parentesco}
							>
								<InputLabel>Grau de parentesco</InputLabel>
								<Select
									id="requerente-parentesco"
									{...field}
									defaultValue={''}
									label="Sexo"
								>
									<MenuItem value={'P'}>Pai</MenuItem>
									<MenuItem value={'M'}>Mãe</MenuItem>
									<MenuItem value={'O'}>Outro</MenuItem>
								</Select>
								<FormHelperText>
									{!!errors.requerente?.parentesco &&
										errors?.requerente?.parentesco?.message}
								</FormHelperText>
							</FormControl>
						)}
					/>
				</Grid>
				{watchParentesco === 'O' && (
					<Fragment>
						<Grid item xs={12} md={8}>
							<Controller
								name="requerente.nome"
								control={control}
								defaultValue={''}
								rules={{ required: false }}
								render={({ field }) => (
									<TextField
										error={!!errors.requerente?.nome}
										helperText={errors?.requerente?.nome?.message}
										label="Nome Completo"
										id="requerente-nome"
										variant="outlined"
										fullWidth
										{...field}
									/>
								)}
							/>
						</Grid>

						<Grid item xs={12} md>
							<Controller
								name="requerente.cpf"
								control={control}
								rules={{ required: false }}
								render={({ field }) => (
									<TextField
										error={!!errors.requerente?.cpf}
										helperText={errors?.requerente?.cpf?.message}
										label="CPF"
										id="requerente-cpf"
										variant="outlined"
										fullWidth
										InputProps={{
											inputComponent: InputCPFMask as any,
										}}
										{...field}
									/>
								)}
							/>
						</Grid>
						<Grid item xs={12} md={4}>
							<Controller
								name="requerente.telefone"
								control={control}
								rules={{ required: false }}
								render={({ field }) => (
									<TextField
										error={!!errors.requerente?.telefone}
										helperText={errors?.requerente?.telefone?.message}
										label="Telefone"
										id="requerente-telefone"
										variant="outlined"
										fullWidth
										InputProps={{
											inputComponent: InputTelefoneMask as any,
										}}
										{...field}
									/>
								)}
							/>
						</Grid>
						<Grid item xs={12} md={4}>
							<Controller
								name="requerente.email"
								control={control}
								rules={{ required: false }}
								render={({ field }) => (
									<TextField
										error={!!errors.requerente?.email}
										helperText={errors?.requerente?.email?.message}
										label="E-mail"
										id="requerente-email"
										variant="outlined"
										fullWidth
										{...field}
									/>
								)}
							/>
						</Grid>
					</Fragment>
				)}
			</Grid>
		</Container>
	) : (
		<Fragment />
	);
};

export default RequerenteForm;
