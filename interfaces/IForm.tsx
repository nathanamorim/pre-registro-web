import { UseFormReset, UseFormSetValue } from 'react-hook-form';
import { UseFormWatch } from 'react-hook-form';
import { Control, DeepMap, FieldError } from 'react-hook-form';

export interface IForm {
	errors: DeepMap<any, FieldError>;
	control: Control<any>;
	active: Function;
	current: number;
	watch: UseFormWatch<any>;
}

export interface IFormWithSetValue extends IForm {
	setValue: UseFormSetValue<any>;
}

export interface IFormWithReset extends IForm {
	reset: UseFormReset<any>;
}

export interface IFormItem {
	errors: DeepMap<any, FieldError>;
	control: Control<any>;
	watch: UseFormWatch<any>;
}
export interface IFormItemWithSetValue extends IFormItem {
	setValue: UseFormSetValue<any>;
	id?: number | null;
}

export interface IFieldsItemWithSetValue extends IFormItem {
	setValue: UseFormSetValue<any>;
	id?: number | null;
	node?: string | null;
}

export interface IFormFields extends IFormItem {
	node: string;
	id?: number | null;
}

export interface IFormFieldsWithLabel extends IFormItem {
	node: string;
	id?: number | null;
	label: string;
}

export interface IFormItemWithReset extends IFormItem {
	reset: UseFormReset<any>;
}
export interface IDynamicFormItem extends IFormItem {
	id: number;
}
