import { IAvos } from './IAvos';
import { IFiliacao } from './IFiliacao';
import { IRegistrado } from './IRegistrado';
import { IRequerente } from './IRequerente';

export * from './IRegistrado';
export * from './IRequerente';
export * from './IFiliacao';
export * from './IAvos';

export interface INascimento {
	requerente: IRequerente;
	registrado: IRegistrado;
	filiacao: IFiliacao;
	avos: IAvos;
	chave: string;
}
