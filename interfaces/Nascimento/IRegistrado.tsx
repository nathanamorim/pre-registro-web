import { EnumSexo } from '../../shared/enum';

export interface IRegistrado {
	dnv: string;
	nome: string;
	sexo?: EnumSexo;
	dataNascimento?: Date;
	gemeos: boolean;
	tipoLocal?: number;
	endereco: string;
	naturalidade?: number;
}
