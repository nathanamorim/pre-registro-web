export interface IRequerente {
	nome?: string;
	cpf?: string;
	email?: string;
	telefone?: string;
	parentesco?: string;
}
