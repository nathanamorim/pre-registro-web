import { IPessoa } from '../IPessoa';
export interface IFiliacao {
	mae?: IPessoa;
	pai?: IPessoa;
}
