export interface ILocalCasamento {
	local: string;
	nomeIgreja: string;
	enderecoIgreja: string;
	dataCasamento: Date;
	regimeBens: string;
}
