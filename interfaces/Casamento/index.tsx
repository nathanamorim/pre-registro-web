import { IFiliacao } from '../Nascimento';
import { ILocalCasamento } from './ILocalCasamento';
import { ITestemunha } from './ITestemunha';

export interface ICasamento {
	pretendentes: [];
	filiacao: IFiliacao;
	localCasamento: ILocalCasamento;
	testemunhas: ITestemunha[];
}
