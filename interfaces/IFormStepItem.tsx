export interface IFormStepItem {
	label: string;
	route: string;
}
