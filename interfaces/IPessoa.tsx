import { EnumSexo } from '../shared/enum';

export interface IPessoa {
	cpf?: string;
	rg?: string;
	nome?: string;
	sexo?: EnumSexo;
	dataNascimento: string;
	profissao?: string;
	telefone?: string;
	email?: string;
	naturalidade?: string;
	nacionalidade?: string;
	falecida?: boolean;
	dataFalecimento?: string;
}
