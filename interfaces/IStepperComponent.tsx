import { IFormStepItem } from './IFormStepItem';

export interface IStepperComponent {
	formSteps: IFormStepItem[];
	step: number;
}

export interface IMStepperComponent extends IStepperComponent {
	next: any;
	nextDisabled?: boolean;
	prev: any;
}
